<?php

//file : app/config/constants.php



return [



    'softDataupload' => 'Soft Data Upload',

    'inScanhub' => 'In Scan - HUB',

    'baggingCreated' => 'Bagging Created',

    'inTransit' => 'In Transit',

    'bagVerified' => 'Bag Verified',

    'inScanbranch' => 'Shipment Received At BRANCH',

    'outScanbranch' => 'Out for Delivery',

    'deliveryStatus1' => 'Delivered',    

    'deliveryStatus2' => 'Cancelled',    

    'deliveryStatus3' => 'Undelivered',    

    'deliveryStatus4' => 'Others',

    'reattemptBranch' => 'Reattempt by Branch',

    'rtoProcess'      => 'Process to be RTO',               

    'rtoInitiated'    => 'RTO Initiated',

    'rtoInitiatedbyBranch' => 'RTO - Pending To Transit',

    'rtoIntransittoHub'  => 'RTO - Transit to HUB',

    'rtoVerifiedbyHub' => 'RTO - Received By HUB',    

    'rtoCompletedbyHub' => 'RTO OFD',

    'rtoFinalstatus'    => 'RTO Delivered',     

	'notCollected'    => 'Not Collected',
	
	'collected'    => 'Collected',
	
	'inScanhubRev' => 'Shipment Received At HUB',
	
	'outScanToClient' => 'Out For Delivery To Client From HUB',
	
	'reverseFinalstatus'    => 'Reverse Final Status In HUB'

];