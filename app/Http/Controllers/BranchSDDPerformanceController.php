<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Consignment;
use App\ConsignmentUpdate;
use App\Customer;
use App\BranchPincode;
use App\User;
use App\Drs;

use Illuminate\Support\Facades\Input;
use DB;
use Auth;

use Excel;


class BranchSDDPerformanceController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
	
	public function index()
	{
	   $current_date = \Carbon\Carbon::today()->toDateString();
	   
	   if(!empty(Input::get('branch')))
	   {
		   $branch = Input::get('branch');
	   }
	   if(!empty(Input::get('from_date')))
	   {
		   $from_date = Input::get('from_date');
	   }
	   if(!empty(Input::get('to_date')))
	   {
		   $to_date = Input::get('to_date');
	   }
	   
	   if(!empty($from_date) && !empty($to_date) && !empty($branch))
	   {
		  if($from_date == $to_date)
		  {
		    $current_date = $from_date;
			$shipment_recieved_results = \DB::table('consignment_updates')
										 ->select(\DB::raw('count(distinct consignment_id) as shipment_recieved'), 'location')
										 ->where('current_status', 'Shipment Received At BRANCH')
										 ->where(\DB::raw('date(created_at)'), $current_date)
										 ->where('location', $branch)
										 ->get();	

      
          $ofd_results = \DB::table('consignment_updates')	
				 ->select(\DB::raw('count(distinct consignment_id) as ofd'), 'location')
				 ->where('current_status', 'Out for delivery')
				 ->where(\DB::raw('date(created_at)'), $current_date)
				 ->where('location', $branch)
				 ->get();
				 
				 
		  $delivered_results = \DB::table('consignment_updates')
								 ->select(\DB::raw('count(distinct consignment_id ) as delivered'), 'location')
								 ->where('current_status', 'Delivered')
								 ->where(\DB::raw('date(created_at)'), $current_date)	
								 ->where('location', $branch)
								 ->get(); 		

	
	      $fresh_delivered_results = \DB::table('consignment_updates')
								 ->select(\DB::raw('count(distinct consignment_id) as fresh_delivered'), 'location')
								 ->where('current_status', 'Delivered')
								 ->whereIn('consignment_id', function($query) use($current_date, $branch) {
									   $query->select(\DB::raw('distinct consignment_id'))
											 ->from('consignment_updates')
											 ->where('current_status','Shipment Received At BRANCH')
											 ->where('location', $branch)
											 ->where(\DB::raw('date(created_at)'), $current_date);					 
								   })
								  ->where(\DB::raw('date(created_at)'), $current_date)
								  ->where('location', $branch) 
								  ->get(); 	

			

	       $old_delivered_results = \DB::table('consignment_updates')
								 ->select(\DB::raw('count(distinct consignment_id) as old_delivered'), 'location')
								 ->where('current_status', 'Delivered')
								 ->whereIn('consignment_id', function($query) use($current_date, $branch) {
									   $query->select(\DB::raw('distinct consignment_id'))
											 ->from('consignment_updates')
											 ->where('current_status','Shipment Received At BRANCH')
											 ->where('location', $branch)
											 ->where(\DB::raw('date(created_at)'), '<', $current_date);					 
								   })
								  ->where(\DB::raw('date(created_at)'), $current_date)
								  ->where('location', $branch)
								  ->get();
		  }
          else
		  {
			echo "OOPS For Now Only single Day Report can be generated...<br> Report for more than one day is under constuction...";  
			die;
		  }			  
	   }
	   else if(!empty($from_date) && !empty($to_date) && empty($branch))
	   {
		  if($from_date == $to_date)
		  {
			$current_date = $from_date;
            $shipment_recieved_results = \DB::table('consignment_updates')
										 ->select(\DB::raw('count(distinct consignment_id) as shipment_recieved'), 'location')
										 ->where('current_status', 'Shipment Received At BRANCH')
										 ->where(\DB::raw('date(created_at)'), $current_date)
										 ->groupBy('location')
										 ->get();	

      
          $ofd_results = \DB::table('consignment_updates')	
				 ->select(\DB::raw('count(distinct consignment_id) as ofd'), 'location')
				 ->where('current_status', 'Out for delivery')
				 ->where(\DB::raw('date(created_at)'), $current_date)
				 ->groupBy('location')
				 ->get();
			
          $delivered_results = \DB::table('consignment_updates')
								 ->select(\DB::raw('count(distinct consignment_id ) as delivered'), 'location')
								 ->where('current_status', 'Delivered')
								 ->where(\DB::raw('date(created_at)'), $current_date)	
								 ->groupBy('location')
								 ->get(); 		

	
	      $fresh_delivered_results = \DB::table('consignment_updates')
								 ->select(\DB::raw('count(distinct consignment_id) as fresh_delivered'), 'location')
								 ->where('current_status', 'Delivered')
								 ->whereIn('consignment_id', function($query) use($current_date) {
									   $query->select(\DB::raw('distinct consignment_id'))
											 ->from('consignment_updates')
											 ->where('current_status','Shipment Received At BRANCH')
											 ->where(\DB::raw('date(created_at)'), $current_date);					 
								   })
								  ->where(\DB::raw('date(created_at)'), $current_date)
								  ->groupBy('location') 
								  ->get(); 	

			

	       $old_delivered_results = \DB::table('consignment_updates')
								 ->select(\DB::raw('count(distinct consignment_id) as old_delivered'), 'location')
								 ->where('current_status', 'Delivered')
								 ->whereIn('consignment_id', function($query) use($current_date) {
									   $query->select(\DB::raw('distinct consignment_id'))
											 ->from('consignment_updates')
											 ->where('current_status','Shipment Received At BRANCH')
											 ->where(\DB::raw('date(created_at)'), '<', $current_date);					 
								   })
								  ->where(\DB::raw('date(created_at)'), $current_date)
								  ->groupBy('location') 
								  ->get();  			
		  }		
          else
		  {
			echo "OOPS For Now Only single Day Report can be generated...<br> Report for more than one day is under constuction...";  
			die;  
		  }			  
	   }
	   else if((empty($from_date) || empty($to_date)) && !empty($branch))
	   {
            
		  $shipment_recieved_results = \DB::table('consignment_updates')
										 ->select(\DB::raw('count(distinct consignment_id) as shipment_recieved'), 'location')
										 ->where('current_status', 'Shipment Received At BRANCH')
										 ->where(\DB::raw('date(created_at)'), $current_date)
										 ->where('location', $branch)
										 ->get();	
		 
      
          $ofd_results = \DB::table('consignment_updates')	
				 ->select(\DB::raw('count(distinct consignment_id) as ofd'), 'location')
				 ->where('current_status', 'Out for delivery')
				 ->where(\DB::raw('date(created_at)'), $current_date)
				 ->where('location', $branch)
				 ->get();
				 
				 
		  $delivered_results = \DB::table('consignment_updates')
								 ->select(\DB::raw('count(distinct consignment_id ) as delivered'), 'location')
								 ->where('current_status', 'Delivered')
								 ->where(\DB::raw('date(created_at)'), $current_date)	
								 ->where('location', $branch)
								 ->get(); 		

	
	      $fresh_delivered_results = \DB::table('consignment_updates')
								 ->select(\DB::raw('count(distinct consignment_id) as fresh_delivered'), 'location')
								 ->where('current_status', 'Delivered')
								 ->whereIn('consignment_id', function($query) use($current_date, $branch) {
									   $query->select(\DB::raw('distinct consignment_id'))
											 ->from('consignment_updates')
											 ->where('current_status','Shipment Received At BRANCH')
											 ->where('location', $branch)
											 ->where(\DB::raw('date(created_at)'), $current_date);					 
								   })
								  ->where(\DB::raw('date(created_at)'), $current_date)
								  ->where('location', $branch) 
								  ->get(); 	

			

	       $old_delivered_results = \DB::table('consignment_updates')
								 ->select(\DB::raw('count(distinct consignment_id) as old_delivered'), 'location')
								 ->where('current_status', 'Delivered')
								 ->whereIn('consignment_id', function($query) use($current_date, $branch) {
									   $query->select(\DB::raw('distinct consignment_id'))
											 ->from('consignment_updates')
											 ->where('current_status','Shipment Received At BRANCH')
											 ->where('location', $branch)
											 ->where(\DB::raw('date(created_at)'), '<', $current_date);					 
								   })
								  ->where(\DB::raw('date(created_at)'), $current_date)
								  ->where('location', $branch)
								  ->get();
	   }
	   else
	   {
		  	$shipment_recieved_results = \DB::table('consignment_updates')
										 ->select(\DB::raw('count(distinct consignment_id) as shipment_recieved'), 'location')
										 ->where('current_status', 'Shipment Received At BRANCH')
										 ->where(\DB::raw('date(created_at)'), $current_date)
										 ->groupBy('location')
										 ->get();	

      
          $ofd_results = \DB::table('consignment_updates')	
				 ->select(\DB::raw('count(distinct consignment_id) as ofd'), 'location')
				 ->where('current_status', 'Out for delivery')
				 ->where(\DB::raw('date(created_at)'), $current_date)
				 ->groupBy('location')
				 ->get();
				 
				 
		  $delivered_results = \DB::table('consignment_updates')
								 ->select(\DB::raw('count(distinct consignment_id ) as delivered'), 'location')
								 ->where('current_status', 'Delivered')
								 ->where(\DB::raw('date(created_at)'), $current_date)	
								 ->groupBy('location')
								 ->get(); 		

	
	      $fresh_delivered_results = \DB::table('consignment_updates')
								 ->select(\DB::raw('count(distinct consignment_id) as fresh_delivered'), 'location')
								 ->where('current_status', 'Delivered')
								 ->whereIn('consignment_id', function($query) use($current_date) {
									   $query->select(\DB::raw('distinct consignment_id'))
											 ->from('consignment_updates')
											 ->where('current_status','Shipment Received At BRANCH')
											 ->where(\DB::raw('date(created_at)'), $current_date);					 
								   })
								  ->where(\DB::raw('date(created_at)'), $current_date)
								  ->groupBy('location') 
								  ->get(); 	

			

	       $old_delivered_results = \DB::table('consignment_updates')
								 ->select(\DB::raw('count(distinct consignment_id) as old_delivered'), 'location')
								 ->where('current_status', 'Delivered')
								 ->whereIn('consignment_id', function($query) use($current_date) {
									   $query->select(\DB::raw('distinct consignment_id'))
											 ->from('consignment_updates')
											 ->where('current_status','Shipment Received At BRANCH')
											 ->where(\DB::raw('date(created_at)'), '<', $current_date);					 
								   })
								  ->where(\DB::raw('date(created_at)'), $current_date)
								  ->groupBy('location') 
								  ->get(); 
	   }
		
	    
								  
								  
	$all_data = array();
    
    if(!empty($branch))
	{
		   $all_data[strtolower($branch)]['shipment_recieved'] = 0;
		   $all_data[strtolower($branch)]['ofd'] = 0;
		   $all_data[strtolower($branch)]['delivered'] = 0;
		   $all_data[strtolower($branch)]['fresh_delivered'] = 0;
		   $all_data[strtolower($branch)]['old_delivered'] = 0;
		   
		   $users_results = \DB::table('users')
							->select('username')
							->where('user_type', '4')
							->where('is_searchable', '1')
							->orderBy('username')
							->get();
							
	}
	else
	{
	  $users_results = \DB::table('users')
							->select('username')
							->where('user_type', '4')
							->where('is_searchable', '1')
							->orderBy('username')
							->get();
	  foreach($users_results as $row)
		{
		   $all_data[strtolower($row->username)]['shipment_recieved'] = 0;
		   $all_data[strtolower($row->username)]['ofd'] = 0;
		   $all_data[strtolower($row->username)]['delivered'] = 0;
		   $all_data[strtolower($row->username)]['fresh_delivered'] = 0;
		   $all_data[strtolower($row->username)]['old_delivered'] = 0;
		}	
	}
	
	foreach($shipment_recieved_results as $row)
	{
	   if(!empty($row->shipment_recieved))
	   {
	     $all_data[strtolower($row->location)]['shipment_recieved'] = $row->shipment_recieved;	
	   }
	}
	
	foreach($ofd_results as $row)
	{
	   if(!empty($row->ofd))
	   {
	     $all_data[strtolower($row->location)]['ofd'] = $row->ofd;	
	   }
	}
	
	foreach($delivered_results as $row)
	{
      if(!empty($row->delivered))
	  {	  
	    $all_data[strtolower($row->location)]['delivered'] = $row->delivered;	
	  }
	}
	
	foreach($fresh_delivered_results as $row)
	{
	   if(!empty($row->fresh_delivered))
	   {
	     $all_data[strtolower($row->location)]['fresh_delivered'] = $row->fresh_delivered;	
	   }
	}
	
	foreach($old_delivered_results as $row)
	{
	   if(!empty($row->old_delivered))
	   {
	     $all_data[strtolower($row->location)]['old_delivered'] = $row->old_delivered;	
	   }
	}
	
	return view('branch_sdd_performance.index', compact('all_data', 'users_results', 'from_date', 'to_date', 'branch'));
  }
  
  
  
  
  
  
  public function exportxls(Request $request)
  {
		ini_set('max_execution_time', 1200);
		ini_set('memory_limit', '512M');
        
		$current_date = \Carbon\Carbon::today()->toDateString();
	   
	    if(!empty(Input::get('branch')))
	    {
		   $branch = Input::get('branch');
	    }
	    if(!empty(Input::get('from_date')))
	    {
		   $from_date = Input::get('from_date');
	    }
	    if(!empty(Input::get('to_date')))
	    {
		   $to_date = Input::get('to_date');
	    }
		
			
            
		if(!empty($from_date) && !empty($to_date) && !empty($branch))
	    {
		  if($from_date == $to_date)
		  {
		    $current_date = $from_date;
			$shipment_recieved_results = \DB::table('consignment_updates')
										 ->select(\DB::raw('count(distinct consignment_id) as shipment_recieved'), 'location')
										 ->where('current_status', 'Shipment Received At BRANCH')
										 ->where(\DB::raw('date(created_at)'), $current_date)
										 ->where('location', $branch)
										 ->get();	

      
          $ofd_results = \DB::table('consignment_updates')	
				 ->select(\DB::raw('count(distinct consignment_id) as ofd'), 'location')
				 ->where('current_status', 'Out for delivery')
				 ->where(\DB::raw('date(created_at)'), $current_date)
				 ->where('location', $branch)
				 ->get();
				 
				 
		  $delivered_results = \DB::table('consignment_updates')
								 ->select(\DB::raw('count(distinct consignment_id ) as delivered'), 'location')
								 ->where('current_status', 'Delivered')
								 ->where(\DB::raw('date(created_at)'), $current_date)	
								 ->where('location', $branch)
								 ->get(); 		

	
	      $fresh_delivered_results = \DB::table('consignment_updates')
								 ->select(\DB::raw('count(distinct consignment_id) as fresh_delivered'), 'location')
								 ->where('current_status', 'Delivered')
								 ->whereIn('consignment_id', function($query) use($current_date, $branch) {
									   $query->select(\DB::raw('distinct consignment_id'))
											 ->from('consignment_updates')
											 ->where('current_status','Shipment Received At BRANCH')
											 ->where('location', $branch)
											 ->where(\DB::raw('date(created_at)'), $current_date);					 
								   })
								  ->where(\DB::raw('date(created_at)'), $current_date)
								  ->where('location', $branch) 
								  ->get(); 	

			

	       $old_delivered_results = \DB::table('consignment_updates')
								 ->select(\DB::raw('count(distinct consignment_id) as old_delivered'), 'location')
								 ->where('current_status', 'Delivered')
								 ->whereIn('consignment_id', function($query) use($current_date, $branch) {
									   $query->select(\DB::raw('distinct consignment_id'))
											 ->from('consignment_updates')
											 ->where('current_status','Shipment Received At BRANCH')
											 ->where('location', $branch)
											 ->where(\DB::raw('date(created_at)'), '<', $current_date);					 
								   })
								  ->where(\DB::raw('date(created_at)'), $current_date)
								  ->where('location', $branch)
								  ->get();
		  }
          else
		  {
			echo "OOPS For Now Only single Day Report can be generated...<br> Report for more than one day is under constuction...";  
			die;
		  }			  
	    }
	    else if(!empty($from_date) && !empty($to_date) && empty($branch))
	    {
		 
		  if($from_date == $to_date)
		  {
			
			$current_date = $from_date;
            $shipment_recieved_results = \DB::table('consignment_updates')
										 ->select(\DB::raw('count(distinct consignment_id) as shipment_recieved'), 'location')
										 ->where('current_status', 'Shipment Received At BRANCH')
										 ->where(\DB::raw('date(created_at)'), $current_date)
										 ->groupBy('location')
										 ->get();	

      
            $ofd_results = \DB::table('consignment_updates')	
				 ->select(\DB::raw('count(distinct consignment_id) as ofd'), 'location')
				 ->where('current_status', 'Out for delivery')
				 ->where(\DB::raw('date(created_at)'), $current_date)
				 ->groupBy('location')
				 ->get();
				
              //print_r($ofd_results);die;				
				 
		    $delivered_results = \DB::table('consignment_updates')
								 ->select(\DB::raw('count(distinct consignment_id ) as delivered'), 'location')
								 ->where('current_status', 'Delivered')
								 ->where(\DB::raw('date(created_at)'), $current_date)	
								 ->groupBy('location')
								 ->get(); 		

	
	        $fresh_delivered_results = \DB::table('consignment_updates')
								 ->select(\DB::raw('count(distinct consignment_id) as fresh_delivered'), 'location')
								 ->where('current_status', 'Delivered')
								 ->whereIn('consignment_id', function($query) use($current_date) {
									   $query->select(\DB::raw('distinct consignment_id'))
											 ->from('consignment_updates')
											 ->where('current_status','Shipment Received At BRANCH')
											 ->where(\DB::raw('date(created_at)'), $current_date);					 
								   })
								  ->where(\DB::raw('date(created_at)'), $current_date)
								  ->groupBy('location') 
								  ->get(); 	

			

	         $old_delivered_results = \DB::table('consignment_updates')
								 ->select(\DB::raw('count(distinct consignment_id) as old_delivered'), 'location')
								 ->where('current_status', 'Delivered')
								 ->whereIn('consignment_id', function($query) use($current_date) {
									   $query->select(\DB::raw('distinct consignment_id'))
											 ->from('consignment_updates')
											 ->where('current_status','Shipment Received At BRANCH')
											 ->where(\DB::raw('date(created_at)'), '<', $current_date);					 
								   })
								  ->where(\DB::raw('date(created_at)'), $current_date)
								  ->groupBy('location') 
								  ->get();  			
		  }		
          else
		  {
			echo "OOPS For Now Only single Day Report can be generated...<br> Report for more than one day is under constuction...";  
			die;  
		  }			  
	    }
	    else if((empty($from_date) || empty($to_date)) && !empty($branch))
	    {
            
		  $shipment_recieved_results = \DB::table('consignment_updates')
										 ->select(\DB::raw('count(distinct consignment_id) as shipment_recieved'), 'location')
										 ->where('current_status', 'Shipment Received At BRANCH')
										 ->where(\DB::raw('date(created_at)'), $current_date)
										 ->where('location', $branch)
										 ->get();	
		 
      
          $ofd_results = \DB::table('consignment_updates')	
				 ->select(\DB::raw('count(distinct consignment_id) as ofd'), 'location')
				 ->where('current_status', 'Out for delivery')
				 ->where(\DB::raw('date(created_at)'), $current_date)
				 ->where('location', $branch)
				 ->get();
				 
				 
		  $delivered_results = \DB::table('consignment_updates')
								 ->select(\DB::raw('count(distinct consignment_id ) as delivered'), 'location')
								 ->where('current_status', 'Delivered')
								 ->where(\DB::raw('date(created_at)'), $current_date)	
								 ->where('location', $branch)
								 ->get(); 		

	
	      $fresh_delivered_results = \DB::table('consignment_updates')
								 ->select(\DB::raw('count(distinct consignment_id) as fresh_delivered'), 'location')
								 ->where('current_status', 'Delivered')
								 ->whereIn('consignment_id', function($query) use($current_date, $branch) {
									   $query->select(\DB::raw('distinct consignment_id'))
											 ->from('consignment_updates')
											 ->where('current_status','Shipment Received At BRANCH')
											 ->where('location', $branch)
											 ->where(\DB::raw('date(created_at)'), $current_date);					 
								   })
								  ->where(\DB::raw('date(created_at)'), $current_date)
								  ->where('location', $branch) 
								  ->get(); 	

			

	       $old_delivered_results = \DB::table('consignment_updates')
								 ->select(\DB::raw('count(distinct consignment_id) as old_delivered'), 'location')
								 ->where('current_status', 'Delivered')
								 ->whereIn('consignment_id', function($query) use($current_date, $branch) {
									   $query->select(\DB::raw('distinct consignment_id'))
											 ->from('consignment_updates')
											 ->where('current_status','Shipment Received At BRANCH')
											 ->where('location', $branch)
											 ->where(\DB::raw('date(created_at)'), '<', $current_date);					 
								   })
								  ->where(\DB::raw('date(created_at)'), $current_date)
								  ->where('location', $branch)
								  ->get();
	   }
	   else
	   {
		  	$shipment_recieved_results = \DB::table('consignment_updates')
										 ->select(\DB::raw('count(distinct consignment_id) as shipment_recieved'), 'location')
										 ->where('current_status', 'Shipment Received At BRANCH')
										 ->where(\DB::raw('date(created_at)'), $current_date)
										 ->groupBy('location')
										 ->get();	

      
			  $ofd_results = \DB::table('consignment_updates')	
					 ->select(\DB::raw('count(distinct consignment_id) as ofd'), 'location')
					 ->where('current_status', 'Out for delivery')
					 ->where(\DB::raw('date(created_at)'), $current_date)
					 ->groupBy('location')
					 ->get();
				 
				 
			  $delivered_results = \DB::table('consignment_updates')
									 ->select(\DB::raw('count(distinct consignment_id ) as delivered'), 'location')
									 ->where('current_status', 'Delivered')
									 ->where(\DB::raw('date(created_at)'), $current_date)	
									 ->groupBy('location')
									 ->get(); 		

	
			  $fresh_delivered_results = \DB::table('consignment_updates')
									 ->select(\DB::raw('count(distinct consignment_id) as fresh_delivered'), 'location')
									 ->where('current_status', 'Delivered')
									 ->whereIn('consignment_id', function($query) use($current_date) {
										   $query->select(\DB::raw('distinct consignment_id'))
												 ->from('consignment_updates')
												 ->where('current_status','Shipment Received At BRANCH')
												 ->where(\DB::raw('date(created_at)'), $current_date);					 
									   })
									  ->where(\DB::raw('date(created_at)'), $current_date)
									  ->groupBy('location') 
									  ->get(); 	

			

			   $old_delivered_results = \DB::table('consignment_updates')
									 ->select(\DB::raw('count(distinct consignment_id) as old_delivered'), 'location')
									 ->where('current_status', 'Delivered')
									 ->whereIn('consignment_id', function($query) use($current_date) {
										   $query->select(\DB::raw('distinct consignment_id'))
												 ->from('consignment_updates')
												 ->where('current_status','Shipment Received At BRANCH')
												 ->where(\DB::raw('date(created_at)'), '<', $current_date);					 
									   })
									  ->where(\DB::raw('date(created_at)'), $current_date)
									  ->groupBy('location') 
									  ->get(); 
	   }


            $all_data = array();
    
			if(!empty($branch))
			{
				   $all_data[strtolower($branch)]['shipment_recieved'] = 0;
				   $all_data[strtolower($branch)]['ofd'] = 0;
				   $all_data[strtolower($branch)]['delivered'] = 0;
				   $all_data[strtolower($branch)]['fresh_delivered'] = 0;
				   $all_data[strtolower($branch)]['old_delivered'] = 0;
				   
			}
			else
			{
			  $users_results = \DB::table('users')
									->select('username')
									->where('user_type', '4')
									->where('is_searchable', '1')
									->orderBy('username')
									->get();
			  foreach($users_results as $row)
				{
				   $all_data[strtolower($row->username)]['shipment_recieved'] = 0;
				   $all_data[strtolower($row->username)]['ofd'] = 0;
				   $all_data[strtolower($row->username)]['delivered'] = 0;
				   $all_data[strtolower($row->username)]['fresh_delivered'] = 0;
				   $all_data[strtolower($row->username)]['old_delivered'] = 0;
				}	
			}

			foreach($shipment_recieved_results as $row)
			{
			   if(!empty($row->shipment_recieved))
			   {
				 $all_data[strtolower($row->location)]['shipment_recieved'] = $row->shipment_recieved;	
			   }
			}

			foreach($ofd_results as $row)
			{
			   if(!empty($row->ofd))
			   {
				 $all_data[strtolower($row->location)]['ofd'] = $row->ofd;	
			   }
			}

			foreach($delivered_results as $row)
			{
			  if(!empty($row->delivered))
			  {	  
				$all_data[strtolower($row->location)]['delivered'] = $row->delivered;	
			  }
			}

			foreach($fresh_delivered_results as $row)
			{
			   if(!empty($row->fresh_delivered))
			   {
				 $all_data[strtolower($row->location)]['fresh_delivered'] = $row->fresh_delivered;	
			   }
			}

			foreach($old_delivered_results as $row)
			{
			   if(!empty($row->old_delivered))
			   {
				 $all_data[strtolower($row->location)]['old_delivered'] = $row->old_delivered;	
			   }
			}	   
			
			return Excel::create('sdd_performace_data', function($excel) use ($all_data) {
            																			
					$excel->sheet('datas', function($sheet) use($all_data)
					{
						$sdd_array = array();
						$i=0; 
						
						foreach($all_data as $key=>$value)
						{
							 $performance;
							 if($value['shipment_recieved']==0)
							 {
								  $performance = '-';
							 }
							  else if($value['shipment_recieved'] > 0)
							  {
								  $performance = ( $value['fresh_delivered'] / $value['shipment_recieved']) * 100;
								  $performance =  round($performance,2,PHP_ROUND_HALF_DOWN).'%';
							  }
							 array_push($sdd_array, [$i, $key, $value['shipment_recieved'], $value['ofd'], $value['delivered'], $value['fresh_delivered'], $value['old_delivered'], $performance, '80%']);
							 
							 $i++;		
						}
					
				
                    $sheet->fromArray($sdd_array, null, 'A1', false, false);
                // Add before first row

									
					$sheet->prependRow(1, array(
                    'S.No', 
					'Branch',
					'Shipment Receieved',
					'OFD',
                    'Delivered',
					'Old Delivered',
					'Fresh Delivered', 
					'SDD Performance',
					'Target',
					));

				    $sheet->cells('A1:AH1', function($cells) {
				
					// manipulate the range of cells
						   
				// Set font
								$cells->setFont(array(
									'family'     => 'Calibri',
									'size'       => '11',
									'bold'       =>  true,
									'background' => 'blue'
								));    
								
								// Set all borders (top, right, bottom, left)
								$cells->setBorder('solid', 'none', 'none', 'solid');

			
                 });
            })->download('xls');
	 });
}
  
  
  
}
?>