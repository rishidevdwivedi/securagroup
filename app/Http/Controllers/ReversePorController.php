<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ReverseConsignment;
use App\ReverseConsignmentUpdate;
use Illuminate\Support\Facades\Input;

use App\ReversePor;
use App\ReverseDrs;
use Auth;

class ReversePorController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }    

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        //
        if(Input::get('date')){
            $date = Input::get('date');
        }else{
            $date = \Carbon\Carbon::today()->toDateString();
        }


        //
        $pods = ReversePor::whereDate('created_at', $date)->orderBy('updated_at', 'DESC')->where('created_by', Auth::user()->username)->paginate(10);
        return view('reverse-por.index', compact('pods'));                

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('reverse-por.create');                
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $this->validate($request, [
            'por_code' => 'required|unique:reverse_por'
        ]);


        ReversePor::create($request->all());
        return redirect('reverse-por');        

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $pod = ReversePor::findorFail($id);
        return view('reverse-por.edit', compact('pod'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //

        $pod = ReversePor::findorFail($id);
        $pod->update($request->all());
        return redirect('reverse-por');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $pod = ReversePor::findorFail($id);
        $pod->forceDelete();
        return redirect('reverse-por');
    }

    public function createBagging(){

        //
        $bags = Drs::all();        
        return view('drs.createbagging',compact('bags'));

    }


    public function storeBagging(Request $request){


        $awbs = explode(PHP_EOL, $request->awbs);

        foreach ($awbs as $awb) {

            $consignment = Consignment::has('consignment_updates', '>=', 1)->with('consignment_updates')->where('awb', $awb)->first();
            $status = array();

            if(isset($consignment->consignment_updates)){

                foreach($consignment->consignment_updates as $update){
                    $status[] = $update->current_status;                    
                }

                if(!in_array(\Config::get('constants.baggingCreated'), $status) && in_array(\Config::get('constants.inScanhub'), $status)){
                ConsignmentUpdate::create([
                    'consignment_id' => $consignment->id,
                    'last_updated_on' => \Carbon\Carbon::now()->toDateString(),
                    'location' => 'New Delhi',
                    'last_updated_by' => Auth::user()->username,
                    'current_status' => \Config::get('constants.baggingCreated'),
                    'remarks' => '',
                    'bag_code' => $request->bag_code,
                    'drs_code' => ''
                ]);

                $consignment = Consignment::findorFail($consignment->id);
                $consignment->update([
                    'last_updated_on' => \Carbon\Carbon::now()->toDateString(),
                    'last_updated_by' => Auth::user()->username,
                    'current_status' => \Config::get('constants.baggingCreated'),
                    'prev_status' => \Config::get('constants.inScanhub'),
                    'bag_code' => $request->bag_code
                ]);
                \Session::flash('success_message','Bagging Created successfully.'); //<--FLASH MESSAGE
                }else { \Session::flash('error_message',"Consignment AWB : $awb  has not been Verified at Origin <a href='/consignment-verification'>RTO Shipment Verification</a>"); }
            }else{ \Session::flash('error_message',"No Consignment Found with AWB : $awb"); }
        }

        return redirect('drs/create-bagging');


    }


}
