<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Consignment;
use App\ConsignmentUpdate;
use App\Customer;
use App\BranchPincode;
use App\User;
use App\Drs;

use Illuminate\Support\Facades\Input;
use DB;
use Auth;

use Excel;


class RiderPerformanceController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
	
	public function index()
	{
		$users_results = \DB::table('users')
							->select('username')
							->where('user_type', '4')
							->where('is_searchable', '1')
							->orderBy('username')
							->get();
		
		return view('rider_performance.index', compact('users_results'));
	}
	
	
}
?>