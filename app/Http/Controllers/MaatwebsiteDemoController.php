<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;
use App\Consignment;
use App\ConsignmentUpdate;
use App\ReverseConsignment;
use App\ReverseConsignmentUpdate;
use App\BranchPincode;
use App\Customer;
use App\User;
use Excel;
use Auth;
use DB;

class MaatwebsiteDemoController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    //

	/**
     * Return View file
     *
     * @var array
     */
	public function importExport()
	{
		return view('consignments.importExport');
	}
	
	public function importTemp()
	{
		return view('consignments.importTemp');
	}
	
	public function reverseImportExport()
	{
		return view('reverse-consignments.reverseImportExport');
	}

	/**
     * File Export Code
     *
     * @var array
     */
	public function downloadExcel(Request $request)
	{
		$customers = User::where('user_type', '5')->get();
		return view('consignments.downloadExcel', compact('customers'));
		//return view('consignments.downloadExcel');
	}
	 
	public function downloadExcelType(Request $request)
	{
		ini_set('max_execution_time', 1200);
		ini_set('memory_limit', '512M');
        $date_from = Input::get('date_from');
        $date_to = Input::get('date_to');
		$MyDateCarbon = \Carbon\Carbon::parse($date_to);
		$date_to = $MyDateCarbon->addDays(1)->toDateString();
		
		$customer =  Input::get('customer_name');
		//echo $customer; die;
		
			$columns = [
			'awb',
			'order_id',
			'item_description',
			'weight', 
			'pcs',
			'payment_mode',
			'collectable_value',
			'price',
			'customer_name',
			'consignee',
			'consignee_address',
			'destination_city',
			'pincode',
			'mobile',
			'branch',
			'current_status',
			'created_at',
			'updated_at'
			];
		DB::enableQueryLog();
		
		if($customer=="all")
			$results = Consignment::whereDate('created_at', '>=' ,$date_from)->whereDate('created_at', '<' ,$date_to)->get($columns);
		else
        	$results = Consignment::whereDate('created_at', '>=' ,$date_from)->whereDate('created_at', '<' ,$date_to)->where('customer_name', $customer)->get($columns);
//dd(DB::getQueryLog());
		if (!$results->count()) {
			return back()->with('error','No Data Found.');
		}
		$data = $results->toArray();
			return Excel::create('consignments_data', function($excel) use ($data) {
            																			
            $excel->sheet('data', function($sheet) use ($data)
            {
				
                $sheet->fromArray($data, null, 'A1', false, false);
                // Add before first row

					$sheet->prependRow(1, array(
                    'Air waybill No', 
                    'Reference No',
                    'Item Description',
                    'Weight(kg)',
                    'Quantity',
					'Payment Mode',
                    'COD Amount',
                    'Declared Value',
                    'Shipper ( Client Name )',
                    'Consignee Name',
                    'Consignee Address',
					'Destination City',
					'Pincode',
                    'Consignee Contact',
					'Branch',
                    'Current Status',
                    'Created Date',
                    'Updated Date',
                ));
                

				$sheet->cells('A1:AH1', function($cells) {
				
					// manipulate the range of cells
						   
				// Set font
				$cells->setFont(array(
					'family'     => 'Calibri',
					'size'       => '11',
					'bold'       =>  true,
					'background' => 'blue'
				));    
				
				// Set all borders (top, right, bottom, left)
				$cells->setBorder('solid', 'none', 'none', 'solid');

});
            });
        })->download('xls');
	}

	/**
     * Import file into database Code
     *
     * @var array
     */
	public function importExcel(Request $request)
	{ //die("faraz");
		ini_set('max_execution_time', 1200);
		ini_set('memory_limit', '512M');
		if($request->hasFile('import_file')){
			$path = $request->file('import_file')->getRealPath();

			$data = Excel::load($path, function($reader) {})->get();
			
			if(!empty($data) && $data->count()){
				
				$org_arr = $data->toArray();
				
				$temp = $duplicates = array();
				foreach($org_arr as $org)
				{
					if(in_array($org['awb'], $temp))
						$duplicates[] = $org['awb'];
					$temp[] = $org['awb'];
					
				}
				
				if(!empty($duplicates))
				{
					$trimmedArray = array_map('trim', $duplicates);
					$duplicates = array_filter($trimmedArray);
					
					foreach($duplicates as $value)
					{
						foreach($org_arr as $subKey => $subArray)
						{
							  if($subArray["awb"] == trim($value))
							  {
								  unset($org_arr[$subKey]);
							  }
						 }
					}
				}
				
				
				$date = \Carbon\Carbon::today()->subDays(30)->toDateString();
				
				//DB::enableQueryLog();

				
				$consignments = Consignment::select('awb')->whereDate('created_at','>=', $date)->orderBy('updated_at','desc')->get();
				
				//dd(DB::getQueryLog());
				
				$table_data = $consignments->toArray();
				
				//echo "<pre>";print_r($table_data);die;
				
				foreach($table_data as $key => $value)
				{
					foreach($org_arr as $subKey => $subArray)
					{
						  if($subArray["awb"] == trim($value['awb']))
						  {
							  $duplicates[] = trim($value['awb']);
							  unset($org_arr[$subKey]);
						  }
					 }
					
				} 
				//echo "<pre>";print_r($org_arr);die;
				//echo "<pre>";print_r($duplicates);die;
			if(!empty($org_arr))
			{	
				$mydata[] = $org_arr;
			
				foreach ($mydata as $key => $value) {
					
					if(!empty($value)){

						foreach ($value as $v) {

							if(isset($v['awb']) && !empty($v['awb'])):

								$branch = BranchPincode::where('pincode', $v['consignee_pincode'])->first();	
								if(isset($branch)){
									$branch = $branch->branch;
									if($branch == '' || $branch == 'ODA')
										$branch = 'Other';
								}else{
									$branch = 'Other';
								}				
									$insert[] = [
									   'data_received_date' => \Carbon\Carbon::parse($v['shipment_datedd_mm_yyyy'])->toDateString(),
									   'awb' => trim($v['awb']),
							           'order_id'  => $v['shippers_reference_number'],
							           'customer_name' => strtoupper($v['customer_code']),
							           'collectable_value' => $v['cod_amount'],
							           'payment_mode' => $v['payment_mode'],
							           'item_description' => $v['content_description'],
							           'consignee' => $v['consignee_name'],
							           'consignee_address' => $v['consignee_address_line_1'] . ' ' . $v['consignee_address_line_2'] . ' ' . $v['consignee_address_line_3'] . ' ' . $v['consignee_address_line_4'],
							           'destination_city' => $v['consignee_city'],
							           'pincode' => $v['consignee_pincode'],
							           'state' => '',
							           'mobile' => $v['consignee_phone_number'] . ' / ' . $v['consignee_mobile'],
							           'current_status' => 'Soft Data Upload',
							           'prev_status' => '',
							           'prev_sub_status' => '',					           
							           'last_updated_on' => \Carbon\Carbon::now()->toDateString(),
					        			'last_updated_by' => Auth::user()->username . ' - API',
							           'pcs' => $v['piece'],
							           'weight' => $v['box_weightkgs'],
							           'price' => $v['declared_value'],
							           'branch' => $branch
									];
							endif;
						}
					}
				}
				if(!empty($insert)){

					foreach ($insert as $in) {
						$consignment = Consignment::create($in);
						$insertedId = $consignment->id;

						ConsignmentUpdate::create([
							'consignment_id' => $insertedId,
					        'last_updated_on' => \Carbon\Carbon::now()->toDateString(),
					        'location' => 'New Delhi',
					        'last_updated_by' => Auth::user()->username . ' - API',
					        'current_status' => 'Soft Data Upload',
					        'remarks' => '',
					        'bag_code' => '',
					        'drs_code' => ''

						]);

					}
					if(!empty($duplicates))
					{
						$awb="";
						foreach($duplicates as $value)
							$awb.= "$value, ";
						$awb = substr($awb, 0, -2);	
						return back()->with('success','Records Inserted Successfully. Duplicates found and ignored - '.$awb.'.');
					}

					return back()->with('success','Records Inserted Successfully.');
				}
			}
				$awb="";
				foreach($duplicates as $value)
					$awb.= "$value, ";
				$awb = substr($awb, 0, -2);
				return back()->with('success','Duplicates found and ignored - '.$awb.'.');
			}

		}

		return back()->with('error','Please Check your file, Something is wrong there.');
	}

	public function importExcelTemp(Request $request)
	{

		if($request->hasFile('import_file')){
			$path = $request->file('import_file')->getRealPath();

			$data = Excel::load($path, function($reader) {})->get();
			if(!empty($data) && $data->count()){
				

			$mydata[] = $data->toArray();
				foreach ($mydata as $key => $value) {
					if(!empty($value)){

						foreach ($value as $v) {
							if(isset($v['awb']) && !empty($v['awb'])):
								Consignment::WithoutTimestamps()->where('awb', $v['awb'])->update(['order_id' => $v['order']]);
							endif;
						}
					}
				}

					return back()->with('success','Records Inserted Successfully.');

			}

		}

		return back()->with('error','Please Check your file, Something is wrong there.');
	}



	public function reverseImportExcel(Request $request)
	{

		if($request->hasFile('import_file')){
			$path = $request->file('import_file')->getRealPath();

			$data = Excel::load($path, function($reader) {})->get();
			//dd($temp);
			if(!empty($data) && $data->count()){
//echo "<pre>"; print_r($data->toArray()); die;
			$temp = $data->toArray();
			$temp2 = $temp[0]['air_waybill_number'];

			if (ReverseConsignment::where('awb', '=', $temp2)->exists()) {
   				return back()->with('error','Duplicate Soft Data Entry.');
			}
			else
			{

			$mydata[] = $data->toArray();
				foreach ($mydata as $key => $value) {
					if(!empty($value)){

						foreach ($value as $v) {

							if(isset($v['air_waybill_number']) && !empty($v['air_waybill_number'])):

								$branch = BranchPincode::where('pincode', $v['pincode'])->first();	
								if(isset($branch)){
									$branch = $branch->branch;
									if($branch == '' || $branch == 'ODA')
										$branch = 'Other';
								}else{
									$branch = 'Other';
								}				
//echo "<pre>"; 
//echo $dt = \Carbon\Carbon::parse($v['date_ddmmyyyy'])->toDateString(); die;
									$insert[] = [
									   //'data_received_date' => \Carbon\Carbon::now()->toDateString(), 
									   'data_received_date' => \Carbon\Carbon::parse($v['dateddmmyyyy'])->toDateString(),
									   'awb' => trim($v['air_waybill_number']),
							           'order_number'  => $v['order_numberref_no.'],
							           'customer_name' => $v['customer_code'],
									  //'customer_code' => $v['customer_code'],
									   'product' => $v['product'],
									   'consignee' => $v['consignee'],
									   'consignee_address' => $v['consignee_address1'] . ' ' . $v['consignee_address2'] . ' ' . $v['consignee_address3'],
									   'destination_city' => $v['destination_city'],
									   'pincode' => $v['pincode'],
									   'state' => $v['state'],
									   'mobile' => $v['mobile'] . ' / ' . $v['telephone'],
									   'item_description' => $v['item_description'],
									   'pcs' => $v['pieces'],
									   'collectable_value' => $v['collectable_value'],
									   'declared_value' => $v['declared_value'],
									   'actual_weight' => $v['actual_weightg'],
									   'volumetric_weight' => $v['volumetric_weightg'],
									   'length' => $v['lengthcms'],
									   'breadth' => $v['breadthcms'],
									   'height' => $v['heightcms'],
									   'issue_category' => $v['issue_category'],
									   'merchant_id' => $v['merchant_id'],
									   'merchant_name' => $v['merchant_name'],
									   'merchant_mobile' => $v['merchant_mobile_no'],
									   'merchant_address' => $v['merchant_address'],
									   'merchant_city' => $v['merchant_city'],
									   'merchant_state' => $v['merchant_state'],
									   'merchant_pincode' => $v['merchant_pincode'],
									   'request_id' => $v['reqid'],
									   'request_type' => $v['requesttype'],
									   'reason_of_return' => $v['reason_of_return'],
									   'sku' => $v['sku'],
									   'brand' => $v['brand'],
									   'category' => $v['category'],
									   'size' => $v['size'],
									   'color' => $v['color'],
									   'paytm_code' => $v['paytm_code'],
									   'serial_or_imei_no' => $v['serialorimeino'],
									   'current_status' => 'Soft Data Upload',
									   'prev_status' => '',
									   'branch' => $branch,
							           'last_updated_on' => \Carbon\Carbon::now()->toDateString(),
					        		   'last_updated_by' => Auth::user()->username . ' - API'
							           /*'created_at' =>  \Carbon\Carbon::now()->toDateString(),
							           'updated_at' =>  \Carbon\Carbon::now()->toDateString()*/
									];
							endif;
						}
					}
				}
			}

				if(!empty($insert)){

					foreach ($insert as $in) {
						$consignment = ReverseConsignment::create($in);
						$insertedId = $consignment->id;

						ReverseConsignmentUpdate::create([
							'reverse_consignment_id' => $insertedId,
					        'last_updated_on' => \Carbon\Carbon::now()->toDateString(),
					        'location' => 'New Delhi',
					        'last_updated_by' => Auth::user()->username . ' - API',
					        'current_status' => 'Soft Data Upload',
					        'remarks' => '',
					        'bag_code' => '',
					        'drs_code' => ''

						]);

					}

					return back()->with('success','Insert Record successfully.');
				}

			}

		}

		return back()->with('error','Please Check your file, Something is wrong there.');
	}

/**
   * Push data into database
   *
   * @var array
*/
	public function pushData(Request $request)
	{

		
			$data = $request['data'];

			if(!empty($data) && $data->count()){
//echo "<pre>"; print_r($data); die;			
				foreach ($data as $key => $value) {
					if(!empty($value)){

						foreach ($value as $v) {

							if(isset($v['AirWayBillNO']) && !empty($v['AirWayBillNO'])):

								$branch = BranchPincode::where('pincode', $v['PickVendorPinCode'])->first();	
								if(isset($branch)){
									$branch = $branch->branch;
									if($branch == '' || $branch == 'ODA')
										$branch = 'Other';
								}else{
									$branch = 'Other';
								}				

									$insert[] = [
									   'data_received_date' => \Carbon\Carbon::now()->toDateString(), 
									   //'data_received_date' => \Carbon\Carbon::parse($v['shipment_datedd_mm_yyyy'])->toDateString(),
									   'awb' => $v['AirWayBillNO'],
	           'order_id'  => $v['ManifestID'],
	           'customer_name' => $v['CustomerName'],
	           'collectable_value' => $v['NetPayment'],
	           'payment_mode' => $v['PaymentStatus'],
	           /*'product' => $v['product'],*/
	           'item_description' => $v['ProductDesc'],
	           'consignee' => $v['CustomerName'],
	           'consignee_address' => $v['CustomerAddress'],
	           'destination_city' => $v['CustomerCity'],
	           'pincode' => $v['ZipCode'],
	           'destination_code' => $v['ZipCode'],
	           'state' => $v['CustomerState'],
	           'mobile' => $v['CustomerMobileNo'],
	           'current_status' => 'Soft Data Upload',	           
	           'prev_status' => '',
	           'prev_sub_status' => '',					           
	           //'last_updated_on' => \Carbon\Carbon::now()->toDateString(),
	           'last_updated_on' => \Carbon\Carbon::parse($v['shipment_datedd_mm_yyyy'])->toDateString(),
       			  //'last_updated_by' => Auth::user()->username . ' - API',
							      
		           'pcs' => $v['Quantity'],
		           'weight' => $v['BillableWeight'],
		           'price' => $v['ProductMRP'],


		           'branch' => $branch,
		           'shipper_name'=>$v['PickupVendor'],
							      'shipper_add1'=>$v['PickVendorAddress'],
							      'shipper_pin'=>$v['PickVendorPinCode'],
							      'shipper_tel_no'=>$v['PickVendorPhoneNo'],
							      'shipper_pin'=>$v['PickVendorPinCode'],
							      
		           //'created_at' => \Carbon\Carbon::parse($v['shipment_datedd_mm_yyyy']),
		           //'created_at' =>  \Carbon\Carbon::now()->toDateString(),
		          // 'updated_at' =>  \Carbon\Carbon::now()->toDateString()
									];
									endif;
						}  //End inner loop
					} // end of if condition
				}


				if(!empty($insert)){

					foreach ($insert as $in) {
						$consignment = Consignment::create($in);
						$insertedId = $consignment->id;

						ConsignmentUpdate::create([
							'consignment_id' => $insertedId,
					        'last_updated_on' => \Carbon\Carbon::now()->toDateString(),
					        'location' => 'New Delhi',
					        'last_updated_by' => Auth::user()->username . ' - API',
					        'current_status' => 'Soft Data Upload',
					        'remarks' => '',
					        'bag_code' => '',
					        'drs_code' => ''

						]);

					}

					return back()->with('success','Insert Record successfully.');
				}

			}

		return back()->with('error','Request body was empty.');
	}
	
	public function importPincodes()
	{
		return view('consignments.importPincodes');
	}

	public function uploadPincodes(Request $request)
	{

		if($request->hasFile('import_file')){
			$path = $request->file('import_file')->getRealPath();

			$data = Excel::load($path, function($reader) {})->get();
			if(!empty($data) && $data->count()){
				
				$org_arr = $data->toArray();
				
				$duplicates = array();

				$pinCodes = BranchPincode::all();

				$table_data = $pinCodes->toArray();
				//echo "<pre>"; print_r($org_arr);
				foreach($org_arr as $key => $value)
				{
					foreach($table_data as $y)
					{
						//echo trim($y['pincode']).", ".trim($value['pincode']); die();
						if(trim($y['pincode']) == trim($value['pincode']))
						{
							$duplicates[] = $value['pincode'];
							unset($org_arr[$key]);
						}
					}
				}
				//echo "<pre>"; print_r($duplicates); echo "<hr/>";
			//echo "<pre>"; print_r($org_arr); die;
			$mydata[] = $org_arr;
				foreach ($mydata as $key => $value) {
					if(!empty($value)){

						foreach ($value as $v) {

							if(isset($v['pincode']) && !empty($v['pincode'])):
								//echo trim($v['district']); die();
									$insert[] = [
									   'pincode' => trim($v['pincode']),
									   'state' => trim($v['state']),
							           'district'  => $v['district'],
							           'branch' => $v['branch'],
							           'branch_code' => $v['branch_code'],
							           
									];
							endif;
						}
					}
				}

				if(!empty($insert)){

					foreach ($insert as $in) {
						BranchPincode::create($in);
					}
					
					if(!empty($duplicates))
					{
						$pincodes="";
						foreach($duplicates as $value)
							$pincodes.= "$value, ";
						$pincodes = substr($pincodes, 0, -2);	
						return back()->with('success','Records Inserted Successfully. Duplicates Pin Codes found and ignored - '.$pincodes.'.');
					}

					return back()->with('success','Records Inserted Successfully.');
				}

			}

		}

		return back()->with('error','Please Check your file, Something is wrong there.');
	}
	
	public function importExcelTest(Request $request)
	{ //die("faraz");
		ini_set('max_execution_time', 1200);
		ini_set('memory_limit', '512M');

		if($request->hasFile('import_file')){
			$path = $request->file('import_file')->getRealPath();

			$data = Excel::load($path, function($reader) {})->get();

			if(!empty($data) && $data->count()){
//echo "<pre>"; print_r($data); die;
			$mydata[] = $data->toArray();
				foreach ($mydata as $key => $value) {
					if(!empty($value)){

						foreach ($value as $v) {
//echo "<pre>";print_r($v);die;
//echo $dt = \Carbon\Carbon::parse($v['date_ddmmyyyy'])->toDateString(); die;
							Consignment::where('awb', $v['awb'])->update(['collectable_value' => $v['price']]);

						}
					}
				}
			}
		}

		return back()->with('error','Please Check your file, Something is wrong there.');
	}
}
