<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Consignment;
use App\ConsignmentUpdate;
use App\Drs;
use Illuminate\Support\Facades\Input;
use Excel;
use Charts;
use DB;
use App\User;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
		ini_set('max_execution_time', 1200);
		ini_set('memory_limit', '512M');

	
		$customers = User::where('user_type', '5')->get();
        $branches = User::where('user_type', '4')->where('is_searchable','1')->orderBy('username', 'asc')->get(); 

        if(Input::get('date_from')){
            $date_from = Input::get('date_from');
        }else{
            $date_from = \Carbon\Carbon::today()->toDateString();
        }

        if(Input::get('date_to')){
            $date_to = Input::get('date_to');
        }else{
            $date_to = \Carbon\Carbon::today()->toDateString();
			
        }
		$MyDateCarbon = \Carbon\Carbon::parse($date_to);
		$date_to = $MyDateCarbon->addDays(1)->toDateString();

        $delivered = Consignment::whereDate('updated_at', '>=' ,$date_from)->whereDate('updated_at', '<' ,$date_to)->where('current_status', \Config::get('constants.deliveryStatus1'));
		
        //$undelivered = Consignment::whereDate('updated_at', '>=' ,$date_from)->whereDate('updated_at', '<' ,$date_to)->where('current_status', \Config::get('constants.deliveryStatus3'));
		DB::enableQueryLog();

		
		$undelivered = ConsignmentUpdate::distinct("consignment_id")->whereDate('updated_at', '>=' ,$date_from)->whereDate('updated_at', '<' ,$date_to)->where('current_status', \Config::get('constants.deliveryStatus3'))->orderBy('location');
		//dd(DB::getQueryLog());
		
        $ofd = ConsignmentUpdate::distinct("consignment_id")->whereDate('updated_at', '>=' ,$date_from)->whereDate('updated_at', '<' ,$date_to)->where('current_status', \Config::get('constants.outScanbranch'))->orderBy('location');
		
		$firstAttempt = Consignment::whereDate('updated_at', '>=' ,$date_from)->whereDate('updated_at', '<' ,$date_to)->where('current_status', \Config::get('constants.deliveryStatus1'))->where('no_of_attempts','=','1');
		
		$moreAttempt = Consignment::whereDate('updated_at', '>=' ,$date_from)->whereDate('updated_at', '<' ,$date_to)->where('current_status', \Config::get('constants.deliveryStatus1'))->where('no_of_attempts','>','1');
		
		$codCollected = Consignment::whereDate('updated_at', '>=' ,$date_from)->whereDate('updated_at', '<' ,$date_to)->where('current_status', \Config::get('constants.deliveryStatus1'))->where('payment_mode','COD');
		
		$codPending = Consignment::whereDate('updated_at', '>=' ,$date_from)->whereDate('updated_at', '<' ,$date_to)->where('current_status', \Config::get('constants.outScanbranch'))->where('payment_mode','COD');
		
		$onTimeDelhiNcr = Consignment::whereDate('updated_at', '>=' ,$date_from)->whereDate('updated_at', '<' ,$date_to)->where('current_status', \Config::get('constants.deliveryStatus1'))->whereIn('branch', array('Jasola','Ghaziabad','Rangpuri','Noida','Dwarka','Old Delhi','Faridabad'))->whereRaw('TIMESTAMPDIFF(HOUR, created_at, updated_at) <= 24');
		
		$oneToTwoDaysDelhiNcr = Consignment::whereDate('updated_at', '>=' ,$date_from)->whereDate('updated_at', '<' ,$date_to)->where('current_status', \Config::get('constants.deliveryStatus1'))->whereIn('branch', array('Jasola','Ghaziabad','Rangpuri','Noida','Dwarka','Old Delhi','Faridabad'))->whereRaw('TIMESTAMPDIFF(HOUR, created_at, updated_at) > 24')->whereRaw('TIMESTAMPDIFF(HOUR, created_at, updated_at) <= 48');
		
		$moreThanTwoDaysDelhiNcr = Consignment::whereDate('updated_at', '>=' ,$date_from)->whereDate('updated_at', '<' ,$date_to)->where('current_status', \Config::get('constants.deliveryStatus1'))->whereIn('branch', array('Jasola','Ghaziabad','Rangpuri','Noida','Dwarka','Old Delhi','Faridabad'))->whereRaw('TIMESTAMPDIFF(HOUR, created_at, updated_at) > 48');
		
		$onTimeOustsideDelhi = Consignment::whereDate('updated_at', '>=' ,$date_from)->whereDate('updated_at', '<' ,$date_to)->where('current_status', \Config::get('constants.deliveryStatus1'))->whereNotIn('branch', array('Jasola','Ghaziabad','Rangpuri','Noida','Dwarka','Old Delhi','Faridabad'))->whereRaw('TIMESTAMPDIFF(HOUR, created_at, updated_at) <= 48');
		
		$oneToTwoDaysOustsideDelhi = Consignment::whereDate('updated_at', '>=' ,$date_from)->whereDate('updated_at', '<' ,$date_to)->where('current_status', \Config::get('constants.deliveryStatus1'))->whereNotIn('branch', array('Jasola','Ghaziabad','Rangpuri','Noida','Dwarka','Old Delhi','Faridabad'))->whereRaw('TIMESTAMPDIFF(HOUR, created_at, updated_at) > 48')->whereRaw('TIMESTAMPDIFF(HOUR, created_at, updated_at) <= 72');
		
		$moreThanTwoDaysOustsideDelhi = Consignment::whereDate('updated_at', '>=' ,$date_from)->whereDate('updated_at', '<' ,$date_to)->where('current_status', \Config::get('constants.deliveryStatus1'))->whereNotIn('branch', array('Jasola','Ghaziabad','Rangpuri','Noida','Dwarka','Old Delhi','Faridabad'))->whereRaw('TIMESTAMPDIFF(HOUR, created_at, updated_at) > 72');		

$undeliveredReasons = Consignment::whereDate('updated_at', '>=' ,$date_from)->whereDate('updated_at', '<' ,$date_to)->where('current_status', \Config::get('constants.deliveryStatus3'));
		
		$results = Consignment::whereDate('updated_at', '>=' ,$date_from)->whereDate('updated_at', '<' ,$date_to)->where('current_status', \Config::get('constants.deliveryStatus3'));
		
	
		//dd(DB::getQueryLog());
		
		//$rtoCons = Consignment::with('consignment_updates')->where('current_status', 'like', '%RTO%')->where('current_status', 'not like', 'RTO Final Status');
		
        if($request->has('branch'))
		{
             $delivered = $delivered->where('branch', $request->input('branch'));
			 $undelivered = $undelivered->where('branch', $request->input('branch'));
			 $ofd = $ofd->where('branch', $request->input('branch'));
			 $firstAttempt = $firstAttempt->where('branch', $request->input('branch'));
			 $moreAttempt = $moreAttempt->where('branch', $request->input('branch'));
			 $codCollected = $codCollected->where('branch', $request->input('branch'));
			 $codPending = $codPending->where('branch', $request->input('branch'));
			 /*$onTimeDelhiNcr = $onTimeDelhiNcr->where('branch', $request->input('branch'));
			 $oneToTwoDaysDelhiNcr = $oneToTwoDaysDelhiNcr->where('branch', $request->input('branch'));
			 $moreThanTwoDaysDelhiNcr = $moreThanTwoDaysDelhiNcr->where('branch', $request->input('branch'));
			 $onTimeOustsideDelhi = $onTimeOustsideDelhi->where('branch', $request->input('branch'));
			 $oneToTwoDaysOustsideDelhi = $oneToTwoDaysOustsideDelhi->where('branch', $request->input('branch'));
			 $moreThanTwoDaysOustsideDelhi = $moreThanTwoDaysOustsideDelhi->where('branch', $request->input('branch'));*/
			 $undeliveredReasons = $undeliveredReasons->where('branch', $request->input('branch'));
			 $results = $results->where('branch', $request->input('branch'));
			 //$rtoCons = $rtoCons->where('branch', $request->input('branch'));
		}

        if($request->has('customer_name'))
		{
             $delivered = $delivered->where('customer_name', $request->input('customer_name'));
			 $undelivered = $undelivered->where('customer_name', $request->input('customer_name'));
			 $ofd = $ofd->where('customer_name', $request->input('customer_name'));
			 $firstAttempt = $firstAttempt->where('customer_name', $request->input('customer_name'));
			 $moreAttempt = $moreAttempt->where('customer_name', $request->input('customer_name'));
			 $codCollected = $codCollected->where('customer_name', $request->input('customer_name'));
			 $codPending = $codPending->where('customer_name', $request->input('customer_name'));
			 /*$onTimeDelhiNcr = $onTimeDelhiNcr->where('customer_name', $request->input('customer_name'));
			 $oneToTwoDaysDelhiNcr = $oneToTwoDaysDelhiNcr->where('customer_name', $request->input('customer_name'));
			 $moreThanTwoDaysDelhiNcr = $moreThanTwoDaysDelhiNcr->where('customer_name', $request->input('customer_name'));
			 $onTimeOustsideDelhi = $onTimeOustsideDelhi->where('customer_name', $request->input('customer_name'));
			 $oneToTwoDaysOustsideDelhi = $oneToTwoDaysOustsideDelhi->where('customer_name', $request->input('customer_name'));
			 $moreThanTwoDaysOustsideDelhi = $moreThanTwoDaysOustsideDelhi->where('customer_name', $request->input('customer_name'));*/
			 $undeliveredReasons = $undeliveredReasons->where('customer_name', $request->input('customer_name'));
			 $results = $results->where('customer_name', $request->input('customer_name'));
			 //$rtoCons = $rtoCons->where('customer_name', $request->input('customer_name'));
		}
		
		$delivered = $delivered->count();
		$undelivered = $undelivered->count();
		$ofd = $ofd->count();
		$firstAttempt = $firstAttempt->count();
		$moreAttempt = $moreAttempt->count();
		$codCollected = $codCollected->sum('collectable_value');
		$codPending = $codPending->sum('collectable_value');
		$onTimeDelhiNcr = $onTimeDelhiNcr->count();
		$oneToTwoDaysDelhiNcr = $oneToTwoDaysDelhiNcr->count();
		$moreThanTwoDaysDelhiNcr = $moreThanTwoDaysDelhiNcr->count();
		$onTimeOustsideDelhi = $onTimeOustsideDelhi->count();
		$oneToTwoDaysOustsideDelhi = $oneToTwoDaysOustsideDelhi->count();
		$moreThanTwoDaysOustsideDelhi = $moreThanTwoDaysOustsideDelhi->count();
		$undeliveredReasons = $undeliveredReasons->distinct()->get(['remarks']);
		$results = $results->get();
		//$rtoCons = $rtoCons->get();
		
		$undeliveredRemarks = array();
		foreach($undeliveredReasons as $undeliveredReason)
		{
			$count = 0;
			foreach($results as $result)
				{
					if(strtolower($result->remarks) == strtolower($undeliveredReason->remarks))
						$count = $count + 1;
					
				}
			$undeliveredRemarks["$undeliveredReason->remarks"]["count"] = $count;
		}
		
		
		/*$twoDaysAgedRTO = $moreDaysAgedRTO = 0;
		
		foreach($rtoCons as $rtoCon)
		{
			foreach($rtoCon->consignment_updates as $update)
			{
				if($update->current_status == "Process to be RTO")
				{
					if(strtotime($update->created_at) >= strtotime($aged) and strtotime($update->created_at) < strtotime($aged2))
					{
						$twoDaysAgedRTO = $twoDaysAgedRTO + 1;
					}
					else
					{
						$moreDaysAgedRTO = $moreDaysAgedRTO + 1;
					}
				}
			}
		}*/
        
        return view('home',[
            "delivered"=>$delivered,
            "undelivered"=>$undelivered,
			"ofd"=>$ofd,
			"firstAttempt"=>$firstAttempt,
			"moreAttempt"=>$moreAttempt,
			"codCollected"=>$codCollected,
			"codPending"=>$codPending,
			"onTimeDelhiNcr"=>$onTimeDelhiNcr,
			"oneToTwoDaysDelhiNcr"=>$oneToTwoDaysDelhiNcr,
			"moreThanTwoDaysDelhiNcr"=>$moreThanTwoDaysDelhiNcr,
			"onTimeOustsideDelhi"=>$onTimeOustsideDelhi,
			"oneToTwoDaysOustsideDelhi"=>$oneToTwoDaysOustsideDelhi,
			"moreThanTwoDaysOustsideDelhi"=>$moreThanTwoDaysOustsideDelhi,
			"undeliveredRemarks"=>$undeliveredRemarks,
			//"twoDaysAgedRTO"=>$twoDaysAgedRTO,
			//"moreDaysAgedRTO"=>$moreDaysAgedRTO,
			"customers"=>$customers,
			"branches"=>$branches
            ]);
    }



    public function exportExcel(Request $request){
        
		ini_set('max_execution_time', 1200);
		ini_set('memory_limit', '512M');

		if(Input::get('date_from')){
            $date_from = Input::get('date_from');
        }else{
            $date_from = \Carbon\Carbon::today()->toDateString();
        }

        //
        if(Input::get('date_to')){
            $date_to = Input::get('date_to');
        }else{
            $date_to = \Carbon\Carbon::today()->toDateString();
        }
		
		$MyDateCarbon = \Carbon\Carbon::parse($date_to);
		$date_to = $MyDateCarbon->addDays(1)->toDateString();
		
		
		DB::enableQueryLog();
		
        $type = Input::get('type');
        switch ($type) {
            case 'Delivered':
                # code...
				$columns = [
				'awb',
				'payment_mode',
				'collectable_value',
				'customer_name',
				'consignee',
				'pincode',
				'mobile',
				'created_at',
				'branch',
				'current_status',
				'updated_at',
				'remarks',
				'no_of_attempts',
				];

				$results = Consignment::whereDate('updated_at', '>=' ,$date_from)->whereDate('updated_at', '<' ,$date_to)->where('current_status', \Config::get('constants.deliveryStatus1'))->orderBy('updated_at','desc');
                break;
            case 'Undelivered':
                # code...
				
				$columns = [
				'awb',
				'payment_mode',
				'collectable_value',
				'customer_name',
				'consignee',
				'pincode',
				'mobile',
				'created_at',
				'branch',
				'current_status',
				'updated_at',
				'remarks',
				'no_of_attempts',
				];
	
                $results = Consignment::whereDate('updated_at', '>=' ,$date_from)->whereDate('updated_at', '<' ,$date_to)->where('current_status', \Config::get('constants.deliveryStatus3'))->orderBy('updated_at','desc');
                break;
				
            case 'Un-Attempted':
                # code...
				
				$columns = [
				'awb',
				'payment_mode',
				'collectable_value',
				'customer_name',
				'consignee',
				'pincode',
				'mobile',
				'created_at',
				'branch',
				'current_status',
				'updated_at',
				'remarks',
				'no_of_attempts',
				];
	
                $results = Consignment::whereDate('updated_at', '>=' ,$date_from)->whereDate('updated_at', '<' ,$date_to)->where('current_status', \Config::get('constants.outScanbranch'))->orderBy('updated_at','desc');
                break;
            case 'Collected':
                # code...
				$columns = [
				'awb',
				'payment_mode',
				'collectable_value',
				'branch',
				'current_status',
				'updated_at',
				];

				$results = Consignment::whereDate('updated_at', '>=' ,$date_from)->whereDate('updated_at', '<' ,$date_to)->where('current_status', \Config::get('constants.deliveryStatus1'))->where('payment_mode','COD')->orderBy('updated_at','desc');
                break;
            case 'Pending':
                # code...
				$columns = [
				'awb',
				'payment_mode',
				'collectable_value',
				'branch',
				'current_status',
				'updated_at',
				];

				$results = Consignment::whereDate('updated_at', '>=' ,$date_from)->whereDate('updated_at', '<' ,$date_to)->where('current_status', \Config::get('constants.outScanbranch'))->where('payment_mode','COD')->orderBy('updated_at','desc');
                break;
            case 'undeliveredAnalysis':
                # code...
				$columns = [
				'awb',
				'payment_mode',
				'collectable_value',
				'customer_name',
				'consignee',
				'pincode',
				'mobile',
				'created_at',
				'branch',
				'current_status',
				'updated_at',
				'remarks',
				'no_of_attempts',
				];

				$results = Consignment::whereDate('updated_at', '>=' ,$date_from)->whereDate('updated_at', '<' ,$date_to)->where('current_status', \Config::get('constants.deliveryStatus3'))->where('remarks', 'like', Input::get('undeliveredReason'))->orderBy('updated_at','desc');
                break;
            case 'tatDelhi':
                # code...
				$columns = [
				'awb',
				'customer_name',
				'created_at',
				'branch',
				'current_status',
				];

				if(Input::get('tatStatus')=="On Time")
				{
                	$results = Consignment::whereDate('updated_at', '>=' ,$date_from)->whereDate('updated_at', '<' ,$date_to)->where('current_status', \Config::get('constants.deliveryStatus1'))->whereIn('branch', array('Jasola','Ghaziabad','Rangpuri','Noida','Dwarka','Old Delhi','Faridabad'))->whereRaw('TIMESTAMPDIFF(HOUR, created_at, updated_at) <= 24')->orderBy('updated_at','desc');
				}
				elseif(Input::get('tatStatus')=="1-2 Days")
				{
					$results = Consignment::whereDate('updated_at', '>=' ,$date_from)->whereDate('updated_at', '<' ,$date_to)->where('current_status', \Config::get('constants.deliveryStatus1'))->whereIn('branch', array('Jasola','Ghaziabad','Rangpuri','Noida','Dwarka','Old Delhi','Faridabad'))->whereRaw('TIMESTAMPDIFF(HOUR, created_at, updated_at) > 24')->whereRaw('TIMESTAMPDIFF(HOUR, created_at, updated_at) <= 48')->orderBy('updated_at','desc');
				}
				else
					$results = Consignment::whereDate('updated_at', '>=' ,$date_from)->whereDate('updated_at', '<' ,$date_to)->where('current_status', \Config::get('constants.deliveryStatus1'))->whereIn('branch', array('Jasola','Ghaziabad','Rangpuri','Noida','Dwarka','Old Delhi','Faridabad'))->whereRaw('TIMESTAMPDIFF(HOUR, created_at, updated_at) > 48')->orderBy('updated_at','desc');
                break;
            case 'tatOutsideDelhi':
                # code...
				$columns = [
				'awb',
				'customer_name',
				'created_at',
				'branch',
				'current_status',
				];

				if(Input::get('tatStatus')=="On Time")
				{
                	$results = Consignment::whereDate('updated_at', '>=' ,$date_from)->whereDate('updated_at', '<' ,$date_to)->where('current_status', \Config::get('constants.deliveryStatus1'))->whereNotIn('branch', array('Jasola','Ghaziabad','Rangpuri','Noida','Dwarka','Old Delhi','Faridabad'))->whereRaw('TIMESTAMPDIFF(HOUR, created_at, updated_at) <= 48')->orderBy('updated_at','desc');
				}
				elseif(Input::get('tatStatus')=="2-3 Days")
				{
					$results = Consignment::whereDate('updated_at', '>=' ,$date_from)->whereDate('updated_at', '<' ,$date_to)->where('current_status', \Config::get('constants.deliveryStatus1'))->whereNotIn('branch', array('Jasola','Ghaziabad','Rangpuri','Noida','Dwarka','Old Delhi','Faridabad'))->whereRaw('TIMESTAMPDIFF(HOUR, created_at, updated_at) > 48')->whereRaw('TIMESTAMPDIFF(HOUR, created_at, updated_at) <= 72')->orderBy('updated_at','desc');
				}
				else
					$results = Consignment::whereDate('updated_at', '>=' ,$date_from)->whereDate('updated_at', '<' ,$date_to)->where('current_status', \Config::get('constants.deliveryStatus1'))->whereNotIn('branch', array('Jasola','Ghaziabad','Rangpuri','Noida','Dwarka','Old Delhi','Faridabad'))->whereRaw('TIMESTAMPDIFF(HOUR, created_at, updated_at) > 72')->orderBy('updated_at','desc');
                break;
            default:
                # code...
                break;
        }
		
		//dd(DB::getQueryLog());
		
        if($request->has('branch'))
             $results = $results->where('branch', $request->input('branch'))->orderBy('updated_at', 'desc');

        if($request->has('customer_name'))
             $results = $results->where('customer_name', $request->input('customer_name'))->orderBy('updated_at', 'desc');

		if(Input::get('type') == "Delivered" or Input::get('type') == "Undelivered" or Input::get('type') == "Collected" or Input::get('type') == "Pending" or Input::get('type') == "undeliveredAnalysis" or Input::get('type') == "Un-Attempted")
		{
     	 $consignments = $results->get();
	  
	  		$dev_boy = array();
			foreach($consignments as $result)
			{
				$temp = Drs::select('delivery_boy')->where('id',$result->drs_id)->first();
				if($temp)
				$dev_boy[] = $temp->delivery_boy;
				else
				$dev_boy[] = "";
			}
     		$consignments = $results->get($columns);
			$temps=$consignments->toArray();
			$i=0;
			foreach($temps as &$temp)
			{
				$temp['delivery_boy'] = $dev_boy[$i];
				$i++;
			}
			$data=collect($temps);
		}
		else
		{
			$consignments = $results->get($columns);
			$data=$consignments->toArray();
		}
		

			return Excel::create('consignments_data', function($excel) use ($data) {
            																			
            $excel->sheet('data', function($sheet) use ($data)
            {
				
                $sheet->fromArray($data, null, 'A1', false, false);
                // Add before first row
					if(Input::get('type') == "Delivered" or Input::get('type') == "Undelivered" or Input::get('type') == "undeliveredAnalysis" or Input::get('type') == "Un-Attempted")
					{
						$sheet->prependRow(1, array(
						'Air waybill No', 
						'Payment Mode',
						'COD Amount',
						'Shipper ( Client Name )',
						'Consignee Name',
						'Pincode',
						'Consignee Contact',
						'Process Date',
						'Branch Name',
						'Current Status',
						'Updated Date',
						'Remarks',
						'No Of Attempt',
						'Delivery Boy Name',
					));
					}
					elseif(Input::get('type') == "tatDelhi" or Input::get('type') == "tatOutsideDelhi")
					{
						$sheet->prependRow(1, array(
						'Air waybill No',
						'Shipper ( Client Name )',
						'Created Date',
						'Branch Name',
						'Current Status',
					));
					}
					else
					{
						$sheet->prependRow(1, array(
						'Air waybill No',
						'Payment Mode',
						'COD Amount',
						'Branch Name',
						'Current Status',
						'Updated Date',
						'Delivery Boy Name',
					));
					}

				$sheet->cells('A1:AH1', function($cells) {
				
					// manipulate the range of cells
						   
				// Set font
				$cells->setFont(array(
					'family'     => 'Calibri',
					'size'       => '11',
					'bold'       =>  true,
					'background' => 'blue'
				));    
				
				// Set all borders (top, right, bottom, left)
				$cells->setBorder('solid', 'none', 'none', 'solid');

});
            });
        })->download('xls');

    }


}
