<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ReverseConsignment;
use App\ReverseConsignmentUpdate;
use Illuminate\Support\Facades\Input;

use App\ReverseBag;
use Auth;

class ReverseBaggingController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }    

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        //
        if(Input::get('date')){
            $date = Input::get('date');
        }else{
            $date = \Carbon\Carbon::today()->toDateString();
        }

        $bags = ReverseBag::whereDate('created_at', $date)->orderBy('updated_at', 'DESC')->has('bag_reverse_consignments', '>=', 1)->with('bag_reverse_consignments')->paginate(10);       
        return view('reverse-bagging.index', compact('bags'));                
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $bags = Bag::where('in_transit','!=', 1)->where('is_verified','!=', 1)->get();        
        return view('bagging.create',compact('bags'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
       $awbs = explode(PHP_EOL, $request->awbs);

        foreach ($awbs as $awb) {

            $consignment = Consignment::has('consignment_updates', '>=', 1)->with('consignment_updates')->where('awb', $awb)->first();
            $status = array();

            if(isset($consignment->consignment_updates)){

                foreach($consignment->consignment_updates as $update){
                    $status[] = $update->current_status;                    
                }

                if(!in_array(\Config::get('constants.baggingCreated'), $status) && in_array(\Config::get('constants.inScanhub'), $status)){
                ConsignmentUpdate::create([
                    'consignment_id' => $consignment->id,
                    'last_updated_on' => \Carbon\Carbon::now()->toDateString(),
                    'location' => 'New Delhi',
                    'last_updated_by' => Auth::user()->username,
                    'current_status' => \Config::get('constants.baggingCreated'),
                    'remarks' => '',
                    'bag_code' => $request->bag_code,
                    'drs_code' => ''
                ]);
                $consignment = Consignment::findorFail($consignment->id);
                $consignment->update([
                    'last_updated_on' => \Carbon\Carbon::now()->toDateString(),
                    'last_updated_by' => Auth::user()->username,
                    'current_status' => \Config::get('constants.baggingCreated'),
                    'prev_status' => \Config::get('constants.inScanhub'),
                    'bag_code' => $request->bag_code,
                    'bag_id'   => $request->bag_id
                ]);
                \Session::flash('success_message','Bagging Created successfully.'); //<--FLASH MESSAGE
                }else { \Session::flash('error_message',"Consignment AWB : $awb  has not been Scanned at HUB <a href='/consignment-verification'>In Scan</a>"); }
            }else{ \Session::flash('error_message',"No Consignment Found with AWB : $awb"); }
        }

        return redirect('bagging');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $bag = Bag::with('bag_consignments')->findorFail($id); 
        return view('bagging.edit', compact('bag'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $awb = $request->awb;
        if($request->action){

            $consignment = Consignment::has('consignment_updates', '>=', 1)->with('consignment_updates')->where('awb', $awb)->first();
            $status = array();

            if(isset($consignment->consignment_updates)){

                foreach($consignment->consignment_updates as $update){
                    $status[] = $update->current_status;                    
                }

                if(!in_array(\Config::get('constants.baggingCreated'), $status) && in_array(\Config::get('constants.inScanhub'), $status)){
                ConsignmentUpdate::create([
                    'consignment_id' => $consignment->id,
                    'last_updated_on' => \Carbon\Carbon::now()->toDateString(),
                    'location' => 'New Delhi',
                    'last_updated_by' => Auth::user()->username,
                    'current_status' => \Config::get('constants.baggingCreated'),
                    'remarks' => '',
                    'bag_code' => $request->bag_code,
                    'drs_code' => ''
                ]);
                $consignment = Consignment::findorFail($consignment->id);
                $consignment->update([
                    'last_updated_on' => \Carbon\Carbon::now()->toDateString(),
                    'last_updated_by' => Auth::user()->username,
                    'current_status' => \Config::get('constants.baggingCreated'),
                    'prev_status' => \Config::get('constants.inScanhub'),
                    'bag_code' => $request->bag_code,
                    'bag_id'   => $request->bag_id
                ]);
                \Session::flash('success_message','Bagging Created successfully.'); //<--FLASH MESSAGE
                }else { \Session::flash('error_message',"Consignment AWB : $awb  has not been Scanned at HUB <a href='/consignment-verification'>In Scan</a>"); }
            }else{ \Session::flash('error_message',"No Consignment Found with AWB : $awb"); }

        }else{

            $consignment = Consignment::has('consignment_updates', '>=', 1)->with('consignment_updates')->where('awb', $awb)->first();
            $status = array();

            if(isset($consignment->consignment_updates)){
                foreach($consignment->consignment_updates as $update){
                    $status[] = $update->current_status;                    
                }

                if(in_array(\Config::get('constants.baggingCreated'), $status) && in_array(\Config::get('constants.inScanhub'), $status)){

                    $consignment_update = ConsignmentUpdate::where('consignment_id', $consignment->id)->where('current_status', \Config::get('constants.baggingCreated'));
                    
                    if($consignment_update)
                    $consignment_update->forceDelete();


                    $consignment = Consignment::findorFail($consignment->id);
                    $consignment->update([
                        'last_updated_on' => \Carbon\Carbon::now()->toDateString(),
                        'last_updated_by' => Auth::user()->username,
                        'current_status' => \Config::get('constants.inScanhub'),
                        'prev_status' => \Config::get('constants.softDataupload'),
                        'bag_code' => null,
                        'bag_id'   => null
                    ]);
                    \Session::flash('success_message','Bagging Created successfully.'); //<--FLASH MESSAGE
                }else { 
                    \Session::flash('error_message',"Consignment AWB : $awb  has not been Scanned at HUB <a href='/consignment-verification'>In Scan</a>"); 
                }

                    if($consignment_update)
                    $consignment_update->forceDelete();
            }else{ \Session::flash('error_message',"No Consignment Found with AWB : $awb"); }

        }
        return redirect('bagging');

    }

    public function editStatus($id){

        $bag = Bag::with('bag_consignments')->findorFail($id); 
        return view('bagging.status', compact('bag'));

    }

    public function verifyBag($id){
        $bag = ReverseBag::with('bag_reverse_consignments')->findorFail($id); 
        return view('reverse-bagging.verify', compact('bag'));

    }
    public function updateStatus(Request $request, $id){

        $status = ($request->status)? '1' : '0';
		
		echo $status; die;
                    $bag = ReverseBag::has('bag_reverse_consignments', '>=', 1)->with('bag_reverse_consignments')->findorFail($id);
                    $bag->update([
                        'in_transit' => $status,
                    ]);


        if($status){

            foreach ($bag->bag_consignments as $bag_consignment) {

                $consignment = Consignment::has('consignment_updates', '>=', 1)->with('consignment_updates')->where('awb', $bag_consignment->awb)->first();
                $status = array();

                if(isset($consignment->consignment_updates)){

                    foreach($consignment->consignment_updates as $update){
                        $status[] = $update->current_status;                    
                    }

                    if(!in_array(\Config::get('constants.inTransit'), $status) && in_array(\Config::get('constants.baggingCreated'), $status) && in_array(\Config::get('constants.inScanhub'), $status)){
                    ConsignmentUpdate::create([
                        'consignment_id' => $consignment->id,
                        'last_updated_on' => \Carbon\Carbon::now()->toDateString(),
                        'location' => 'New Delhi',
                        'last_updated_by' => Auth::user()->username,
                        'current_status' => \Config::get('constants.inTransit'),
                        'remarks' => '',
                        'bag_code' => $bag->bag_code,
                        'drs_code' => ''
                    ]);
                    $consignment = Consignment::findorFail($consignment->id);
                    $consignment->update([
                        'last_updated_on' => \Carbon\Carbon::now()->toDateString(),
                        'last_updated_by' => Auth::user()->username,
                        'current_status' => \Config::get('constants.inTransit'),
                        'prev_status' => \Config::get('constants.baggingCreated'),
                        'bag_code' => $bag->bag_code,
                        'bag_id'   => $bag->id
                    ]);
                    \Session::flash('success_message','Bagging In Transit.'); //<--FLASH MESSAGE
                    }else { \Session::flash('error_message',"Consignment AWB :   has not been Scanned at HUB <a href='/consignment-verification'>In Scan</a>"); }
                }else{ \Session::flash('error_message',"No Consignment Found with AWB : "); }
            }

        }else{

            foreach ($bag->bag_consignments as $bag_consignment) {

                $consignment = Consignment::has('consignment_updates', '>=', 1)->with('consignment_updates')->where('awb', $bag_consignment->awb)->first();
                $status = array();

                if(isset($consignment->consignment_updates)){

                    foreach($consignment->consignment_updates as $update){
                        $status[] = $update->current_status;                    
                    }

                    if(in_array(\Config::get('constants.inTransit'), $status) && in_array(\Config::get('constants.baggingCreated'), $status) && in_array(\Config::get('constants.inScanhub'), $status)){
                    
                    $consignment_update = ConsignmentUpdate::where('consignment_id', $consignment->id)->where('current_status', \Config::get('constants.inTransit'));
                    
                    if($consignment_update)
                    $consignment_update->forceDelete();

                    $consignment = Consignment::findorFail($consignment->id);
                    $consignment->update([
                        'last_updated_on' => \Carbon\Carbon::now()->toDateString(),
                        'last_updated_by' => Auth::user()->username,
                        'current_status' => \Config::get('constants.baggingCreated'),
                        'prev_status' => \Config::get('constants.inScanhub'),
                        'bag_code' => $bag->bag_code,
                        'bag_id'   => $bag->id
                    ]);
                    \Session::flash('success_message','Bagging In Transit Removed.'); //<--FLASH MESSAGE
                    }else { \Session::flash('error_message',"Consignment AWB :   has not been Scanned at HUB <a href='/consignment-verification'>In Scan</a>"); }
                }else{ \Session::flash('error_message',"No Consignment Found with AWB : "); }
            }



        }

        return redirect('bagging');


    }


    public function updateBagVerification(Request $request, $id){

        $status = ($request->verify)? '1' : '0';
		//echo $status; die;
                    $bag = ReverseBag::has('bag_reverse_consignments', '>=', 1)->with('bag_reverse_consignments')->findorFail($id);
                    $bag->update([
                        'is_verified' => $status,
                    ]);


        if($status){

            foreach ($bag->bag_reverse_consignments as $bag_consignment) {

                $consignment = ReverseConsignment::with('reverse_consignment_updates')->where('awb', $bag_consignment->awb)->first();
                $status = array();

                if(isset($consignment->reverse_consignment_updates)){

                    foreach($consignment->reverse_consignment_updates as $update){
                        $status[] = $update->current_status;                    
                    }

                    if(!in_array(\Config::get('constants.bagVerified'), $status) && in_array(\Config::get('constants.inTransit'), $status) && in_array(\Config::get('constants.baggingCreated'), $status) && in_array(\Config::get('constants.collected'), $status)){
                    ReverseConsignmentUpdate::create([
                        'reverse_consignment_id' => $consignment->id,
                        'last_updated_on' => \Carbon\Carbon::now()->toDateString(),
                        'location' => 'New Delhi',
                        'last_updated_by' => Auth::user()->username,
                        'current_status' => \Config::get('constants.bagVerified'),
                        'remarks' => '',
                        'bag_code' => $bag->bag_code,
                        'drs_code' => ''
                    ]);
                    $consignment = ReverseConsignment::findorFail($consignment->id);
                    $consignment->update([
                        'last_updated_on' => \Carbon\Carbon::now()->toDateString(),
                        'last_updated_by' => Auth::user()->username,
                        'current_status' => \Config::get('constants.bagVerified'),
                        'prev_status' => \Config::get('constants.inTransit'),
                        'reverse_bag_code' => $bag->bag_code,
                        'reverse_bag_id'   => $bag->id
                    ]);
                    \Session::flash('success_message','Reverse Bagging Verified successfully.'); //<--FLASH MESSAGE
                    }else { \Session::flash('error_message',"Consignment AWB :   has not been In Transited."); }
                }else{ \Session::flash('error_message',"No Consignment Found with AWB : "); }
            }

        }else{

            foreach ($bag->bag_reverse_consignments as $bag_consignment) {

                $consignment = ReverseConsignment::with('reverse_consignment_updates')->where('awb', $bag_consignment->awb)->first();
                $status = array();

                if(isset($consignment->reverse_consignment_updates)){

                    foreach($consignment->reverse_consignment_updates as $update){
                        $status[] = $update->current_status;                    
                    }

                    if(in_array(\Config::get('constants.bagVerified'), $status) && in_array(\Config::get('constants.inTransit'), $status) && in_array(\Config::get('constants.baggingCreated'), $status) && in_array(\Config::get('constants.collected'), $status)){
                    
                    $consignment_update = ReverseConsignmentUpdate::where('reverse_consignment_id', $consignment->id)->where('current_status', \Config::get('constants.bagVerified'));
                    
                    if($consignment_update)
                    $consignment_update->forceDelete();

                    $consignment = ReverseConsignment::findorFail($consignment->id);
                    $consignment->update([
                        'last_updated_on' => \Carbon\Carbon::now()->toDateString(),
                        'last_updated_by' => Auth::user()->username,
                        'current_status' => \Config::get('constants.inTransit'),
                        'prev_status' => \Config::get('constants.baggingCreated'),
                        'reverse_bag_code' => $bag->bag_code,
                        'reverse_bag_id'   => $bag->id
                    ]);
                    \Session::flash('success_message','Bagging Verified Removed.'); //<--FLASH MESSAGE
                    }else { \Session::flash('error_message',"Consignment AWB :   has not been In Transited."); }
                }else{ \Session::flash('error_message',"No Consignment Found with AWB : "); }
            }



        }

        return redirect('reverse-bagging');        

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
