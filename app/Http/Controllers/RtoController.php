<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Consignment;
use App\ConsignmentUpdate;
use App\Drs;
use App\User;
use Illuminate\Support\Facades\Input;

use Auth;
use Excel;


class RtoController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //

        $drs = Drs::all();
        return view('rto.index',compact('drs'));                

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
       $awbs = explode(PHP_EOL, $request->rto_awb);

        foreach ($awbs as $awb) {

            $consignment = Consignment::with('consignment_updates')->with('bag')->with('drs')->where('awb', $awb)->where('branch', Auth::user()->username)->first();
            $status = array();

            if(isset($consignment->consignment_updates)){

                //echo "<pre>"; print_r($consignment); die;
                foreach($consignment->consignment_updates as $update){
                    $status[] = $update->current_status;                    
                }

                if((in_array(\Config::get('constants.deliveryStatus1'), $status) || in_array(\Config::get('constants.deliveryStatus2'), $status) || in_array(\Config::get('constants.deliveryStatus3'), $status) || in_array(\Config::get('constants.deliveryStatus4'), $status)) && in_array(\Config::get('constants.outScanbranch'), $status) && in_array(\Config::get('constants.inScanbranch'), $status) && in_array(\Config::get('constants.bagVerified'), $status) && in_array(\Config::get('constants.inTransit'), $status) && in_array(\Config::get('constants.baggingCreated'), $status) && in_array(\Config::get('constants.inScanhub'), $status)){
                ConsignmentUpdate::create([
                    'consignment_id' => $consignment->id,
                    'last_updated_on' => \Carbon\Carbon::now()->toDateString(),
                    'location' => $consignment->bag->to_branch,
                    'last_updated_by' => Auth::user()->username,
                    'current_status' => $request->rto_reason,
                    'remarks' => $request->remarks,
                    'drs_code' => $consignment->drs_code,
                    'bag_code' => $consignment->bag_code
                ]);
                $consignment = Consignment::findorFail($consignment->id);
                $consignment->update([
                    'last_updated_on' => \Carbon\Carbon::now()->toDateString(),
                    'last_updated_by' => Auth::user()->username,
                    'current_status' => $request->rto_reason,
                    'prev_status' => \Config::get('constants.outScanbranch'),
                    'drs_code' => $consignment->drs_code,
                    'bag_code'        => $consignment->bag_code,
                    'bag_id'          => $consignment->bag_id,
                    'rto_reason' => $request->rto_reason,
                    'rto_awb' => 'RTO-' . $request->rto_awb                    
                ]);
                \Session::flash('success_message','Rto filed Sucessfully.'); //<--FLASH MESSAGE
                }else { \Session::flash('error_message',"Consignment AWB : $awb  has not been Scanned at HUB <a href='/consignment-verification'>In Scan</a>"); }
            }else{ \Session::flash('error_message',"No Consignment Found with AWB : $awb"); }
        }

        return redirect('rto');

    }

    public function listing(Request $request){

        //
		$branches = User::where('user_type', '4')->where('is_searchable','1')->get();  
        if(Input::get('date')){
            $date = Input::get('date');
        }else{
            $date = \Carbon\Carbon::today()->toDateString();
        }
		
		$results = Consignment::whereDate('updated_at', $date)->orderBy('updated_at', 'DESC')->whereNotNull('rto_awb');
       
	   if($request->has('branch'))
             $results = $results->where('branch', $request->input('branch'));


        $rto_consignments = $results->paginate(10);
		
        return view('rto.listing',compact('rto_consignments', 'branches'));                

    }

    public function receive(Request $request, $id){

//echo "<pre>";print_r($request->all());die;
            $consignment = Consignment::with('consignment_updates')->with('bag')->with('drs')->where('id', $id)->first();
            $status = array();

            if(isset($consignment->consignment_updates)){

                foreach($consignment->consignment_updates as $update){
                    $status[] = $update->current_status;                    
                }

                /*if( !in_array(\Config::get('constants.rtoVerifiedbyHub'), $status) &&  in_array(\Config::get('constants.rtoIntransittoHub'), $status) &&  in_array(\Config::get('constants.rtoInitiatedbyBranch'), $status) &&  in_array(\Config::get('constants.rtoInitiated'), $status) && in_array(\Config::get('constants.rtoProcess'), $status) && in_array(\Config::get('constants.deliveryStatus3'), $status) && in_array(\Config::get('constants.outScanbranch'), $status) && in_array(\Config::get('constants.inScanbranch'), $status) && in_array(\Config::get('constants.bagVerified'), $status) && in_array(\Config::get('constants.inTransit'), $status) && in_array(\Config::get('constants.baggingCreated'), $status) && in_array(\Config::get('constants.inScanhub'), $status)){*/
                if(!in_array(\Config::get('constants.rtoVerifiedbyHub'), $status) &&  in_array(\Config::get('constants.rtoIntransittoHub'), $status) &&  in_array(\Config::get('constants.rtoInitiatedbyBranch'), $status) &&  in_array(\Config::get('constants.rtoInitiated'), $status) && in_array(\Config::get('constants.rtoProcess'), $status) && in_array(\Config::get('constants.deliveryStatus3'), $status) && in_array(\Config::get('constants.outScanbranch'), $status) && in_array(\Config::get('constants.inScanbranch'), $status)){
                ConsignmentUpdate::create([
                    'consignment_id' => $consignment->id,
                    'last_updated_on' => \Carbon\Carbon::now()->toDateString(),
                    'location' => ($consignment->customer_name == 'WOW') ? $consignment->branch : $consignment->bag->to_branch,
                    'last_updated_by' => Auth::user()->username,
                    'current_status' => \Config::get('constants.rtoVerifiedbyHub'),
					'remarks' => $consignment->rto_reason,
                    'drs_code' => $consignment->drs_code,
                    'bag_code' => ($consignment->customer_name == 'WOW') ? NULL : $consignment->bag_code
                ]);
                $consignment = Consignment::findorFail($consignment->id);
                $consignment->update([
                    'last_updated_on' => \Carbon\Carbon::now()->toDateString(),
                    'last_updated_by' => Auth::user()->username,
                    'current_status' => \Config::get('constants.rtoVerifiedbyHub'),
                    'prev_status' => \Config::get('constants.rtoIntransittoHub'),
                    'drs_code' => $consignment->drs_code,
                    'bag_code'        => ($consignment->customer_name == 'WOW') ? NULL : $consignment->bag_code,
                    'bag_id'          => ($consignment->customer_name == 'WOW') ? NULL : $consignment->bag_id
                ]);
                \Session::flash('success_message','Rto Sucessfully Verified at Hub.'); //<--FLASH MESSAGE
                }else { \Session::flash('error_message',"RTO can't be in transit to HUb for Consignment with AWB : $awb ! Reason : Consignment is at step : ". $consignment->current_status ." ! please check this consignment History for details"); }
            }else{ \Session::flash('error_message',"No Consignment Found with AWB : $awb"); }

        return redirect('rto/listing');

    }


    public function receiveMultiple(Request $request){
		
		$branches = User::where('user_type', '4')->where('is_searchable','1')->get();
		$awbs = $request->repeat;
		//echo "<pre>"; print_r($ids); die("khan");
		$founds=$notFounds=$has_errors=array();
		foreach($awbs as $awb)
		{
            $consignment = Consignment::with('consignment_updates')->with('bag')->with('drs')->where('awb', trim($awb))->first();
            $status = array();

            if(isset($consignment->consignment_updates)){

                foreach($consignment->consignment_updates as $update){
                    $status[] = $update->current_status;                    
                }

                if( !in_array(\Config::get('constants.rtoVerifiedbyHub'), $status) &&  in_array(\Config::get('constants.rtoIntransittoHub'), $status) &&  in_array(\Config::get('constants.rtoInitiatedbyBranch'), $status) &&  in_array(\Config::get('constants.rtoInitiated'), $status) && in_array(\Config::get('constants.rtoProcess'), $status) && in_array(\Config::get('constants.deliveryStatus3'), $status) && in_array(\Config::get('constants.outScanbranch'), $status) && in_array(\Config::get('constants.inScanbranch'), $status) && in_array(\Config::get('constants.bagVerified'), $status) && in_array(\Config::get('constants.inTransit'), $status) && in_array(\Config::get('constants.baggingCreated'), $status) && in_array(\Config::get('constants.inScanhub'), $status)){
                ConsignmentUpdate::create([
                    'consignment_id' => $consignment->id,
                    'last_updated_on' => \Carbon\Carbon::now()->toDateString(),
                    'location' => $consignment->bag->to_branch,
                    'last_updated_by' => Auth::user()->username,
                    'current_status' => \Config::get('constants.rtoVerifiedbyHub'),
                    'remarks' => $request->remarks,
                    'drs_code' => $consignment->drs_code,
                    'bag_code' => $consignment->bag_code
                ]);
                $consignment = Consignment::findorFail($consignment->id);
                $consignment->update([
                    'last_updated_on' => \Carbon\Carbon::now()->toDateString(),
                    'last_updated_by' => Auth::user()->username,
                    'current_status' => \Config::get('constants.rtoVerifiedbyHub'),
                    'prev_status' => \Config::get('constants.rtoIntransittoHub'),
                    'drs_code' => $consignment->drs_code,
                    'bag_code'        => $consignment->bag_code,
                    'bag_id'          => $consignment->bag_id,
                    'rto_reason' => $request->rto_reason,
                    'rto_awb' => 'RTO-' . $request->rto_awb                    
                ]);
                $founds[]=$awb; //<--FLASH MESSAGE
                }else { $has_errors[]=$awb; }
            }else{ $notFounds[]=$awb; }
		}
		//Session::set('founds', $founds);
        return redirect('rto/listing')->with('founds', $founds)->with('notFounds', $notFounds)->with('has_errors', $has_errors);
		//return view('rto.listing', compact('founds','notFounds','has_errors','branches'));

    }


    public function complete(Request $request, $id){


            $consignment = Consignment::with('consignment_updates')->with('bag')->with('drs')->where('id', $id)->first();
            $status = array();

            if(isset($consignment->consignment_updates)){

                foreach($consignment->consignment_updates as $update){
                    $status[] = $update->current_status;                    
                }

                if( !in_array(\Config::get('constants.rtoCompletedbyHub'), $status) &&   in_array(\Config::get('constants.rtoVerifiedbyHub'), $status) &&  (in_array(\Config::get('constants.rtoInitiatedbyBranch'), $status) . ' - ' . $consignment->branch)  && (in_array(\Config::get('constants.deliveryStatus1'), $status) || in_array(\Config::get('constants.deliveryStatus2'), $status) || in_array(\Config::get('constants.deliveryStatus3'), $status) || in_array(\Config::get('constants.deliveryStatus4'), $status)) && in_array(\Config::get('constants.outScanbranch'), $status) && in_array(\Config::get('constants.inScanbranch'), $status) && in_array(\Config::get('constants.bagVerified'), $status) && in_array(\Config::get('constants.inTransit'), $status) && in_array(\Config::get('constants.baggingCreated'), $status) && in_array(\Config::get('constants.inScanhub'), $status)){
                ConsignmentUpdate::create([
                    'consignment_id'    => $consignment->id,
                    'last_updated_on'   => \Carbon\Carbon::now()->toDateString(),
                    'location'          => $consignment->bag->to_branch,
                    'last_updated_by'   => Auth::user()->username,
                    'current_status'    => \Config::get('constants.rtoCompletedbyHub'),
                    'remarks'           => $request->remarks,
                    'drs_code'          => $consignment->drs_code,
                    'bag_code'          => $consignment->bag_code
                ]);
                $consignment = Consignment::findorFail($consignment->id);
                $consignment->update([
                    'last_updated_on'   => \Carbon\Carbon::now()->toDateString(),
                    'last_updated_by'   => Auth::user()->username,
                    'current_status'    => \Config::get('constants.rtoCompletedbyHub'),
                    'prev_status'       => \Config::get('constants.rtoVerifiedbyHub') . ' - ' . $consignment->branch,
                    'drs_code'          => $consignment->drs_code,
                    'bag_code'          => $consignment->bag_code,
                    'bag_id'            => $consignment->bag_id,
                    'rto_reason'        => $request->rto_reason,
                    'rto_awb'           => 'RTO-' . $request->rto_awb                    
                ]);
                \Session::flash('success_message','Rto Sucessfully Completed at Hub.'); //<--FLASH MESSAGE
                }else { \Session::flash('error_message',"Consignment AWB :   has not been Scanned at HUB <a href='/consignment-verification'>In Scan</a>"); }
            
            }else{ \Session::flash('error_message',"No Consignment Found with AWB : $awb"); }

        return redirect('rto/listing');



    }





    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


    public function upload(){

        return view('rto.upload');                
    
    }

    public function rto_process(Request $request){

        if($request->hasFile('import_file')){

            $path = $request->file('import_file')->getRealPath();
            $data = Excel::load($path, function($reader) {})->get();

            if(!empty($data) && $data->count()){

                // Getting awbs from the sheet
                $mydata[] = $data->toArray();                

                $ids = array();
                foreach ($mydata as $key => $value) {
                    
                    if(!empty($value)){                      

                        foreach ($value as $v) {

                            // Build an ids array 
                            array_push($ids, $v['awb_no']);
                        }

                    }

                }

                // RTO processing for each AWB No

                foreach ($ids as $awb) {
					

                    $consignment = Consignment::with('consignment_updates')->with('bag')->with('drs')->where('awb', $awb)->first();
                    $status = array();

                    if(isset($consignment->consignment_updates)){

                        foreach($consignment->consignment_updates as $update){
                            $status[] = $update->current_status;                    
                        }
						

                        /*if(!in_array(\Config::get('constants.rtoProcess'), $status) && !in_array(\Config::get('constants.deliveryStatus1'), $status) && in_array(\Config::get('constants.deliveryStatus3'), $status) && in_array(\Config::get('constants.outScanbranch'), $status) && in_array(\Config::get('constants.inScanbranch'), $status) && in_array(\Config::get('constants.bagVerified'), $status) && in_array(\Config::get('constants.inTransit'), $status) && in_array(\Config::get('constants.baggingCreated'), $status) && in_array(\Config::get('constants.inScanhub'), $status))*/
                        if(!in_array(\Config::get('constants.rtoProcess'), $status) && !in_array(\Config::get('constants.deliveryStatus1'), $status) && in_array(\Config::get('constants.deliveryStatus3'), $status) && in_array(\Config::get('constants.outScanbranch'), $status) && in_array(\Config::get('constants.inScanbranch'), $status))
							{
                            ConsignmentUpdate::create([
                                'consignment_id' => $consignment->id,
                                'last_updated_on' => \Carbon\Carbon::now()->toDateString(),
								'location' => ($consignment->customer_name == 'WOW') ? $consignment->branch : $consignment->bag->to_branch,
								'drs_code' => $consignment->drs_code,
								'bag_code' => ($consignment->customer_name == 'WOW') ? NULL : $consignment->bag_code,
								'remarks' => $consignment->remarks,
                                'last_updated_by' => Auth::user()->username,
                                'current_status' => \Config::get('constants.rtoProcess')
                            ]);
                            $consignment = Consignment::findorFail($consignment->id);
                            $consignment->update([
                                'last_updated_on' => \Carbon\Carbon::now()->toDateString(),
                                'last_updated_by' => Auth::user()->username,
                                'current_status' => \Config::get('constants.rtoProcess'),
                                'prev_status' => \Config::get('constants.deliveryStatus3'),                    
                                'bag_id'          => ($consignment->customer_name == 'WOW') ? NULL : $consignment->bag_id
                            ]);
                            \Session::flash('success_message','Rto process filed Sucessfully.'); //<--FLASH MESSAGE
                        
                        }else{ 
                            
                            \Session::flash('error_message',"RTO can't be processed for Consignment with AWB : $awb ! Reason : Consignment is at step : ". $consignment->current_status ." ! please check this consignment History for details"); 
                        }
                
                    }else{ 

                        \Session::flash('error_message',"No Consignment Found with AWB : $awb"); 
                    }
                }
            }

        }else{
            \Session::flash('error_message',"No File was Uploaded ! Please select a valid excel file"); 
        }

        return redirect('rto/upload');

    }

    public function final_status(){
        return view('rto.final_status');                
    }

    public function final_status_process(Request $request){

       //
//print_r($request->all()); die();
       //$awbs = explode(PHP_EOL, $request->awbs);
		$hasErrors=$founds=$notFounds=array();
		$awbs = preg_split("/(\r\n|\n|\r)/",$request->awbs);
		$awbs = array_map('trim',$awbs);
//echo "<pre>"; print_r($awbs); die;
        foreach ($awbs as $awb) {

            $consignment = Consignment::with('consignment_updates')->with('bag')->where('awb', $awb)->first();
            $status = array();
//print_r($consignment); die;

            if(isset($consignment->consignment_updates)){

                foreach($consignment->consignment_updates as $update){
                    $status[] = $update->current_status;                    
                }

                if(!in_array(\Config::get('constants.rtoFinalstatus'), $status) && in_array(\Config::get('constants.rtoCompletedbyHub'), $status) &&  in_array(\Config::get('constants.rtoVerifiedbyHub'), $status) &&  in_array(\Config::get('constants.rtoIntransittoHub'), $status) &&  in_array(\Config::get('constants.rtoInitiatedbyBranch'), $status) &&  in_array(\Config::get('constants.rtoInitiated'), $status) && in_array(\Config::get('constants.rtoProcess'), $status) && in_array(\Config::get('constants.deliveryStatus3'), $status)  && in_array(\Config::get('constants.outScanbranch'), $status) && in_array(\Config::get('constants.inScanbranch'), $status)){
					if($request->status == "Return Delivered")
						$current_status = \Config::get('constants.rtoFinalstatus');
					else
						$current_status = "RTO Undelivered";
                ConsignmentUpdate::create([
                    'consignment_id' => $consignment->id,
                    'last_updated_on' => \Carbon\Carbon::now()->toDateString(),
                    'location' => ($consignment->customer_name == 'WOW') ? $consignment->branch : $consignment->bag->to_branch,
                    'last_updated_by' => Auth::user()->username,
                    'current_status' => $current_status,
                    'remarks' => $request->status . ' - '  .$request->remarks,
                    'bag_code' => ($consignment->customer_name == 'WOW') ? NULL : $consignment->bag_code,
					'drs_code' => $consignment->drs_code,
					'bag_code' => $consignment->pod_code
                ]);
                $consignment = Consignment::findorFail($consignment->id);
                $consignment->update([
                    'last_updated_on' => \Carbon\Carbon::now()->toDateString(),
                    'last_updated_by' => Auth::user()->username,
                    'current_status' => $current_status,
                    'prev_status' => \Config::get('constants.rtoCompletedbyHub'),
                    'bag_code'        => ($consignment->customer_name == 'WOW') ? NULL : $consignment->bag_code,
                    'bag_id'          => ($consignment->customer_name == 'WOW') ? NULL : $consignment->bag->id
                ]);  
					$founds[]=$awb; 
                }else { $hasErrors[]=$awb;	}
            }else{ $notFounds[]=$awb; }
        }
		//redirect()->route( 'rto/final-status' )->with($errors, $notFounds, $success);
        //return redirect('rto/final-status')->with($hasErrors, $notFounds, $founds);
		return view('rto.final_status', compact('hasErrors','notFounds','founds'));

    }
	
}
