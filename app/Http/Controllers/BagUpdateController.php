<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Consignment;
use App\ConsignmentUpdate;
use App\Bag;
use Auth;

class BagUpdateController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $bags = Bag::all();        
        return view('bagsupdate.create',compact('bags'));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        /*
       $this->validate($request, [
            'bag_code' => 'required',
            'awbs' => 'required',
        ]);
       */
        $awbs = explode(PHP_EOL, $request->awbs);

        foreach ($awbs as $awb) {

            $consignment = Consignment::has('consignment_updates', '>=', 1)->with('consignment_updates')->where('awb', $awb)->first();
            $status = array();

            if(isset($consignment->consignment_updates)){

                foreach($consignment->consignment_updates as $update){
                    $status[] = $update->current_status;                    
                }

                if(!in_array(\Config::get('constants.baggingCreated'), $status) && in_array(\Config::get('constants.inScanhub'), $status)){
                ConsignmentUpdate::create([
                    'consignment_id' => $consignment->id,
                    'last_updated_on' => \Carbon\Carbon::now()->toDateString(),
                    'location' => 'New Delhi',
                    'last_updated_by' => Auth::user()->username,
                    'current_status' => \Config::get('constants.baggingCreated'),
                    'remarks' => '',
                    'bag_code' => $request->bag_code,
                    'drs_code' => ''
                ]);

                $consignment = Consignment::findorFail($consignment->id);
                $consignment->update([
                    'last_updated_on' => \Carbon\Carbon::now()->toDateString(),
                    'last_updated_by' => Auth::user()->username,
                    'current_status' => \Config::get('constants.baggingCreated'),
                    'prev_status' => \Config::get('constants.inScanhub'),
                    'bag_code' => $request->bag_code
                ]);
                \Session::flash('success_message','Bagging Created successfully.'); //<--FLASH MESSAGE
                }else { \Session::flash('error_message',"Consignment AWB : $awb  has not been Verified at Origin <a href='/consignment-verification'>RTO Shipment Verification</a>"); }
            }else{ \Session::flash('error_message',"No Consignment Found with AWB : $awb"); }
        }

        return redirect('bagupdate/create');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function manage(){

        return view('bagsupdate.manage');

    }

    public function dispatchbag(Request $request){

       $this->validate($request, [
            'bag_code' => 'required',
        ]);

        $consignments = Consignment::has('consignment_updates', '>=', 1)->with('consignment_updates')->where('bag_code', $request->bag_code)->get();
        //echo "<pre>"; print_r($consignments); die;

        foreach ($consignments as $consignment) {

            $status = array();

            if(isset($consignment->consignment_updates)){

                foreach($consignment->consignment_updates as $update){
                    $status[] = $update->current_status;                    
                }

                if(!in_array('In Transit', $status) &&  in_array('Bagging Created', $status) && in_array('Scan at Origin', $status)){
                ConsignmentUpdate::create([
                    'consignment_id' => $consignment->id,
                    'last_updated_on' => \Carbon\Carbon::now()->toDateString(),
                    'location' => 'New Delhi',
                    'last_updated_by' => Auth::user()->username,
                    'current_status' => 'In Transit',
                    'remarks' => '',
                    'bag_code' => $request->bag_code,
                    'drs_code' => ''
                ]);

                $consignment = Consignment::findorFail($consignment->id);
                $consignment->update([
                    'last_updated_on' => \Carbon\Carbon::now()->toDateString(),
                    'last_updated_by' => Auth::user()->username,
                    'current_status' => 'In Transit',
                    'prev_status' => 'Bagging Created',
                    'bag_code' => $request->bag_code
                ]);
                \Session::flash('success_message','Bag is now In Transit successfully.'); //<--FLASH MESSAGE
                }else { \Session::flash('error_message',"Bag Code :   has not been Verified at Origin <a href='/consignment-verification'>RTO Shipment Verification</a>"); }
            }else{ \Session::flash('error_message',"No Bag Found with Bag code : $request->bag_code "); }
        }

        return redirect('bagsupdate/manage');

    }


    public function verify(){

        return view('bagsupdate.verify');

    }


    public function verifybag(Request $request){

       $this->validate($request, [
            'bag_code' => 'required',
        ]);

        $consignments = Consignment::has('consignment_updates', '>=', 1)->with('consignment_updates')->where('bag_code', $request->bag_code)->get();
        //echo "<pre>"; print_r($consignments); die;

        foreach ($consignments as $consignment) {

            $status = array();

            if(isset($consignment->consignment_updates)){

                foreach($consignment->consignment_updates as $update){
                    $status[] = $update->current_status;                    
                }

                if(!in_array('Bag Verified', $status) && in_array('In Transit', $status) &&  in_array('Bagging Created', $status) && in_array('Scan at Origin', $status)){
                ConsignmentUpdate::create([
                    'consignment_id' => $consignment->id,
                    'last_updated_on' => \Carbon\Carbon::now()->toDateString(),
                    'location' => 'New Delhi',
                    'last_updated_by' => Auth::user()->username,
                    'current_status' => 'Bag Verified',
                    'remarks' => '',
                    'bag_code' => $request->bag_code,
                    'drs_code' => ''
                ]);

                $consignment = Consignment::findorFail($consignment->id);
                $consignment->update([
                    'last_updated_on' => \Carbon\Carbon::now()->toDateString(),
                    'last_updated_by' => Auth::user()->username,
                    'current_status' => 'Bag Verified',
                    'prev_status' => 'In Transit',
                    'bag_code' => $request->bag_code
                ]);
                \Session::flash('success_message','Bag Verified successfully.'); //<--FLASH MESSAGE
                }else { \Session::flash('error_message',"Bag Code :   has not been Verified at Origin <a href='/consignment-verification'>RTO Shipment Verification</a> or already verified please check status"); }
            }else{ \Session::flash('error_message',"No Bag Found with Bag code : $request->bag_code "); }
        }

        return redirect('bagsupdate/verify');

    }


}
