<?php



namespace App;



use Illuminate\Database\Eloquent\Model;



class ReverseBag extends Model

{

    //

   //public $fillable = ['bag_code','to_branch','in_transit','bag_id', 'is_verified'];    
   protected $guarded = [];



    public function bag_reverse_consignments()

    {

        return $this->hasMany('App\ReverseConsignment');

    }



}

