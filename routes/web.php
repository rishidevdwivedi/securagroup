<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index');
Route::get('/home/exportExcel', 'HomeController@exportExcel');

Route::get('/aging-reports', 'AgingReportsController@showReports');
Route::get('/aging-reports/exportExcel', 'AgingReportsController@exportExcel');
/*
Route::get('database/clean', 'DatabaseController@clean');
*/
Route::get('consignments/exportExcel', 'ConsignmentController@exportExcel');
Route::get('consignments-aging/exportExcel', 'ConsignmentAgingController@exportExcel');
Route::post('consignments/unlock/{id}', 'ConsignmentController@unlock');
Route::delete('consignments/reset/{id}', 'ConsignmentController@reset');
Route::post('consignments/resetbranch/{id}', 'ConsignmentController@resetbranch');

Route::get('reverse-consignments/exportExcel', 'ReverseConsignmentController@exportExcel');
Route::post('reverse-consignments/repeat/{id}', 'ReverseConsignmentController@repeat');
Route::delete('reverse-consignments/reset/{id}', 'ReverseConsignmentController@reset');
Route::post('reverse-consignments/resetbranch/{id}', 'ReverseConsignmentController@resetbranch');


Route::resource('consignments', 'ConsignmentController');
Route::get('new_consignments/exportxls', 'NewConsignmentController@exportxls');

Route::resource('new_consignments', 'NewConsignmentController');

Route::resource('caller-consignments', 'CallerConsignmentController');
Route::get('misDownload/exportExcel', 'MisController@exportExcel');
Route::resource('mis-download', 'MisController');

Route::get('consignments-aging', 'ConsignmentAgingController@regularConsignments');
Route::get('rto-consignments-aging', 'ConsignmentAgingController@rtoConsignments');
Route::resource('reverse-consignments', 'ReverseConsignmentController');

Route::get('bagging/{id}/editStatus', 'BaggingController@editStatus');
Route::put('/bagging/updateStatus/{id}', 'BaggingController@updateStatus');
Route::get('bagging/{id}/verifyBag', 'BaggingController@verifyBag');
Route::put('/bagging/updateBagVerification/{id}', 'BaggingController@updateBagVerification');
Route::resource('bagging', 'BaggingController');

Route::resource('reverse-bagging', 'ReverseBaggingController');
Route::get('reverse-bagging/{id}/verifyBag', 'ReverseBaggingController@verifyBag');
Route::put('/reverse-bagging/updateBagVerification/{id}', 'ReverseBaggingController@updateBagVerification');

Route::get('/ajax-show-bagging', 'AjaxController@showbagging');
Route::resource('customers', 'CustomerController');


Route::resource('reverse-in-scan', 'ReverseInscanController');
Route::resource('out-scan', 'OutscanController');
Route::resource('out-scan-pod', 'OutscanpodController');

Route::resource('status', 'StatusController');
Route::get('rto/listing', 'RtoController@listing');
Route::post('rto/receive/{id}', 'RtoController@receive');
Route::post('rto/receive-multiple', 'RtoController@receiveMultiple');
Route::post('rto/complete/{id}', 'RtoController@complete');

Route::get('rto/upload', 'RtoController@upload');
Route::post('rto/rto_process', 'RtoController@rto_process');

Route::get('rto/final-status', 'RtoController@final_status');
Route::post('rto/final-status-process', 'RtoController@final_status_process');

Route::resource('rto', 'RtoController');

Route::resource('pod', 'PodController');

Route::resource('reverse-por', 'ReversePorController');
Route::resource('reverse-out-scan-por', 'OutscanporController');
Route::resource('reverse-porsheets', 'PorSheetsController');
Route::get('reverse-final-status', 'ReverseController@final_status');
Route::post('reverse-final-status-process', 'ReverseController@final_status_process');

Route::resource('podsheets', 'PodSheetsController');

Route::resource('bags', 'BagController');
Route::resource('consignment-verification', 'ConsignmentVerificationController');
Route::resource('consignment-verify', 'ConsignmentVerifyController');

Route::get('bagupdate/manage', 'BagUpdateController@manage');
Route::post('bagupdate/dispatchbag', 'BagUpdateController@dispatchbag');
Route::get('bagupdate/verify', 'BagUpdateController@verify');
Route::post('bagupdate/verifybag', 'BagUpdateController@verifybag');
Route::resource('bagupdate', 'BagUpdateController');



Route::match(array('GET', 'POST'),'tracking/consignment', 'TrackingController@consignment');
Route::post('tracking/exportExcel', 'TrackingController@exportExcel');

Route::resource('tracking', 'TrackingController');


Route::get('api/importExport', 'MaatwebsiteDemoController@importExport');
Route::get('api/importTemp', 'MaatwebsiteDemoController@importTemp');
Route::post('api/importExcelTemp', 'MaatwebsiteDemoController@importExcelTemp');
Route::get('api/downloadExcel', 'MaatwebsiteDemoController@downloadExcel');
Route::get('api/downloadExcelType', 'MaatwebsiteDemoController@downloadExcelType');
Route::post('api/importExcel', 'MaatwebsiteDemoController@importExcel');
Route::get('import-pincodes', 'MaatwebsiteDemoController@importPincodes');
Route::post('upload-pincodes', 'MaatwebsiteDemoController@uploadPincodes');

Route::get('api/reverseImportExport', 'MaatwebsiteDemoController@reverseImportExport');
Route::post('api/reverseImportExcel', 'MaatwebsiteDemoController@reverseImportExcel');

Route::get('shipment/history', 'ShipmentHistoryController@index');
Route::post('shipment/importExcel', 'ShipmentHistoryController@importExcel');


Route::get('/ajax-search-consignments', 'AjaxController@searchConsignments');
Route::get('/ajax-verify-consignment', 'AjaxController@verifyConsignment');
Route::get('/ajax-search-reverse-consignments', 'AjaxController@searchReverseConsignments');
Route::get('/ajax-search-rto-consignments', 'AjaxController@searchRtoConsignments');
Route::get('/ajax-get-bag', 'AjaxController@getBag');


Route::get('shipmentTracking', 'ShipmentTrackingController@showData');

Route::get('driver/report', 'DriverController@report');
Route::get('driver/exportxls', 'DriverController@exportxls');
Route::resource('drivers', 'DriverController');

Route::resource('/branchwise-report', 'BranchWiseReportController');
Route::get('/branchwise-report-details', 'BranchWiseReportController@showDetails');

Route::post('barcode-generator', 'BarcodeGeneratorController@generateBarcode');
Route::get('/audit', 'AuditController@index');
Route::get('/inventory_not_scanned', 'AuditController@inventory_not_scanned');
Route::get('/inventory_scanned', 'AuditController@inventory_scanned');
Route::get('/branch_sdd_performance', 'BranchSDDPerformanceController@index');
Route::get('/branch_sdd_performance/export', 'BranchSDDPerformanceController@exportxls');
Route::get('/rider_performance', 'RiderPerformanceController@index');
