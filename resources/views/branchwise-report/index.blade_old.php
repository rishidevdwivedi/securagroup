@extends('layouts.app')
@section('content')
<div class="container-fluid">

              

<div class="row"  id="search-results">
   <div class="col-md-11 col-md-offset-1">
      <div class="row">  
      <form method="GET" action="/branchwise-report" >

<div class="row">
        <div class="col-md-4">

         <div class="form-group">
            <label for="datepicker_from" class="col-md-6 control-label">Updated From Date:</label>
            <div class="col-md-6">
               <input class="form-control" type="text" name="date_from" id="datepicker_from" placeholder="yyyy-mm-dd">
            </div>
         </div>
        </div>
        
        <div class="col-md-4"> 
         <div class="form-group">
            <label for="datepicker_to" class="col-md-6 control-label">Updated To Date:</label>
            <div class="col-md-6">
               <input class="form-control" type="text" name="date_to" id="datepicker_to" placeholder="yyyy-mm-dd">
            </div>
         </div>
        </div>
        
        <div class="col-md-4">
         <div class="form-group">
          <div class="col-md-4">
           <button type="submit" class="btn btn-sm btn-primary btn-create">Filter</button>
          </div>
         </div>
        </div>

</div>
      </form>
      </div>      
      </p>
      <br />
      <div class="panel panel-default panel-table">
      
     <!-- Starts --> <div class="row">
    <div class="col-sm-12 col-sm-offset-0">

      <!-- Nav tabs -->
      <ul class="nav nav-tabs" role="tablist">
        <li role="presentation" class="active"><a href="/joshadelic/pen/WvmePv.html" id="link-1" aria-controls="section-1" role="tab" data-target="#section-1" data-toggle="tab"><strong>Consignments</strong></a></li>
        <li role="presentation"><a href="/joshadelic/pen/LVabRL.html" id="link-2" aria-controls="section-2" role="tab" data-target="#section-2" data-toggle="tab"><strong>Outside Consignments</strong></a></li>
        
      </ul>

      <!-- Tab panes -->
      <div class="tab-content">
        <div role="tabpanel" class="tab-pane active" id="section-1">
        <div class="panel-heading">
            <div class="row">
               <div class="col col-xs-6">
                  <h2 class="panel-title">Branchwise Report</h2>
               </div>
               <div class="col col-xs-6 text-right">
                  <h2 class="panel-title">Total Number Of Shipments Recieved - <strong>{{$totalShpmtRcvd}}</strong></h2>
               </div>
            </div>
         </div>
         <div class="panel-body">
            <table class="table table-striped table-bordered table-list">
               <thead>
                  <tr>
                     <th>Branch</th>
                     <th>Shipment Received</th>
                     <th>OFD/Total OFD</th>
                     <th>Delivered/Total Delivered</th>
                     <th>Undelivered/Total Undelivered</th>
                     <th>Cancelled/Total Cancelled</th>
                     <th>Reattempt</th>
                     <th>RTO</th>
                     <th><em class="fa fa-cog"></em></th>
                  </tr>
               </thead>
               @foreach($branches as $branch)
               <tbody>
                  <tr>
                     <td>{{ ucfirst($branch->branch) }}</td>
                      @foreach($branchStatus as $key => $value)
                          @if($branch->branch == $key)
                             <td>{{$value['Shipment Received']}}</td>
                             <td>{{$value['OFD']}}/{{ $value['Total OFD']}}</td>
                             <td>{{$value['Delivered']}}/{{$value['Total Delivered']}}</td>
                             <td>{{$value['Undelivered']}}/{{$value['Total Undelivered']}}</td>
                             <td>{{$value['Cancelled']}}/{{$value['Total Cancelled']}}</td>
                             <td>{{$value['Reattempt']}}</td>
                             <td>{{$value['Rto']}}</td>
                             <td><a href="/branchwise-report-details?branch={{$branch->username}}&date_from={{$date_from}}&date_to={{$date_to}}">more details..</a></td>
                          @endif
                     @endforeach
                  </tr>
               </tbody>
               @endforeach
            </table>
         </div>
        
        </div>
        <div role="tabpanel" class="tab-pane" id="section-2">
        <div class="panel-heading">
            <div class="row">
               <div class="col col-xs-6">
                  <h2 class="panel-title">Branchwise Report</h2>
               </div>
               <div class="col col-xs-6 text-right">
                  <h2 class="panel-title">Total Number Of Shipments Recieved - <strong>{{$totalOsShpmtRcvd}}</strong></h2>
               </div>
            </div>
         </div>
         <div class="panel-body">
            <table class="table table-striped table-bordered table-list">
               <thead>
                  <tr>
                     <th>Branch</th>
                     <th>Shipment Received</th>
                     <th>OFD/Total OFD</th>
                     <th>Delivered/Total Delivered</th>
                     <th>Undelivered/Total Undelivered</th>
                     <th>In Stock</th>
                  </tr>
               </thead>
               @foreach($branches as $branch)
               <tbody>
                  <tr>
                     <td>{{ ucfirst($branch->branch) }}</td>
                      @foreach($branchStatus as $key => $value)
                          @if($branch->branch == $key)
                             <td>{{$value['Outside Shipment Received']}}</td>
                             <td>{{$value['Outside OFD']}}/{{ $value['Total Outside OFD']}}</td>
                             <td>{{$value['Outside Delivered']}}/{{$value['Total Outside Delivered']}}</td>
                             <td>{{$value['Outside Undelivered']}}/{{$value['Total Outside Undelivered']}}</td>
                             <td><strong>{{$value['In Stock']}}</strong></td>
                          @endif
                     @endforeach
                  </tr>
               </tbody>
               @endforeach
            </table>
         </div>
        
        </div>
        
      </div>
    </div>
  </div> <!-- Ends -->
  
         
         <div class="panel-footer">
            
         </div>
      </div>
   </div>
</div>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script>
   $.noConflict();  //Not to conflict with other scripts
   jQuery(document).ready(function($) {
   
   
         var getUrlParameter = function getUrlParameter(sParam) {
         var sPageURL = decodeURIComponent(window.location.search.substring(1)),
             sURLVariables = sPageURL.split('&'),
             sParameterName,
             i;
   
         for (i = 0; i < sURLVariables.length; i++) {
             sParameterName = sURLVariables[i].split('=');
   
             if (sParameterName[0] === sParam) {
                 return sParameterName[1] === undefined ? true : sParameterName[1];
             }
         }
     };
   
   if(getUrlParameter('date_from')){
   var date_from = getUrlParameter('date_from');
   
   }else{
   var date_from = new Date();
   }
   
   if(getUrlParameter('date_to')){
   var date_to = getUrlParameter('date_to');
   
   }else{
   var date_to = new Date();
   }
   
   
       $( "#datepicker_from" ).datepicker({
           changeMonth: true,
           changeYear: true,
           dateFormat : 'yy-mm-dd',
           defaultDate: new Date(),
       }).datepicker("setDate", date_from);
   
       $( "#datepicker_to" ).datepicker({
           changeMonth: true,
           changeYear: true,
           dateFormat : 'yy-mm-dd',
           defaultDate: new Date(),
       }).datepicker("setDate", date_to);
   
   
     } );
   
   
   $(document).ready(function(){
  $.get('/joshadelic/pen/WvmePv.html', function(data){
    $('#section-1').html(data);
  });
});

$('[data-toggle="tab"]').on('click', function(){
    var $this = $(this),
        source = $this.attr('href'),
        pane = $this.attr('data-target');
  
    if($(pane).is(':empty')) {  // check if pane is empty, if so get data
      $.get(source, function(data) {
          $(pane).html(data);
      });

      $(this).tab('show');
      return false;
    }
});
     
</script>

@endsection