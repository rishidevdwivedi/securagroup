@extends('layouts.app')

@section('content')
<div class="container-fluid">
   <div id="page-wrapper" class="page-wrapper-cls">
      <div id="page-inner">
         <div class="row">
            <div class="col-md-12">
               <div class="alert alert-warning">
                  <div class="panel panel-default">
                     <div class="panel-heading">
                        Step 7 : HUB : In Scan Reverse Consignment
                     </div>
                     <div class="panel-body">
                        <form class="form-horizontal" role="form" method="POST" action="/reverse-in-scan">
                           {{ csrf_field() }}
                           <input type="hidden" name="action"     value="show" />
                           <div class="form-group">
                              <label for="exampleInputEmail1" class="col-md-4 control-label">Waybill</label> 
                              <div class="col-md-6" >
                              <textarea name="txtwaybill" rows="2" cols="20" id="" class="form-control" placeholder="Scan waybill and press submit button." style="height:150px;"></textarea><br>
                              </div>
                           </div>
                           <div class="form-group">
                              <div class="col-md-4" ></div>
                              <div class="col-md-6" >
                              <input type="submit" name="" value="Submit" onclick="" id="" class="btn btn-info">
                              </div>
                           </div>
                        </form>
                     </div>
                  </div>
                  <div class="panel panel-default">
                     <div class="panel-heading">
                        Message Log
                     </div>
                     <div class="panel-body">
                        @if(isset($consignment)) 
                        Consignment Found for Waybill no: {{ $consignment->awb }} <br />
                        @foreach($consignment->reverse_consignment_updates as $update)
                        <?php
                           if($update->current_status == \Config::get('constants.inScanhubRev'))
                             $found = 1;
                           ?>
                        @endforeach
                        @if(!isset($found)) 
                        <form class="form-horizontal" role="form" method="POST" action="/reverse-in-scan">
                           {{ csrf_field() }}
                           <input type="hidden" name="action"     value="verify" />
                           <input type="hidden" name="txtwaybill" value="{{ $consignment->awb }}" />
                           <div class="col col-xs-12 text-left">
                              <button type="submit" class="btn btn-sm btn-success btn-create">Verify In Scan</button>
                           </div>
                        </form>
                        @else
                        Already Verified !
                        @endif
                        @elseif(isset($awb))
                        Consignment was not found for Waybill no : {{ $awb }} 
                        @endif
                        <div class="form-group">
                           <span id="ctl00_ContentPlaceHolder1_lblmsg" style="color:Red;"></span>
                        </div>
                     </div>
                  </div>
                  <!--
                     <div class="panel panel-default">
                                        <div class="panel-heading">
                                          RTO  Consignments Verified Today
                                        </div>
                                        <div class="panel-body">
                                       
                     <div class="form-group">
                      <div>
                     
                     </div>
                     </div>
                     
                     -->
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
</div>
</div>

@endsection
