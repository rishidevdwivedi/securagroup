@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Add New Bag</div>
                <div class="panel-body">
                    <form class="form-horizontal" role="form" method="POST" action="/bags">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('bag_code') ? ' has-error' : '' }}">
                            <label for="bag_code" class="col-md-4 control-label">Bag Code</label>

                            <div class="col-md-6">
                                <input id="bag_code" type="text" class="form-control" name="bag_code" value="{{ old('bag_code') }}" required autofocus>

                                @if ($errors->has('bag_code'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('bag_code') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('to_branch') ? ' has-error' : '' }}">
                            <label for="to_branch" class="col-md-4 control-label">To Branch</label>

                            <div class="col-md-6">
                                <select id="to_branch" class="form-control" name="to_branch" value="{{ old('to_branch') }}" required autofocus required>
                                    <option value="">None</option>

                                    @foreach($branches as $branch)
                                    <option value="{{ ucfirst($branch->username) }}">{{ ucfirst($branch->username) }}</option>
                                    @endforeach

                                    <!--
                                    <option value="Ghaziabad">Ghaziabad</option>
                                    <option value="Rangpuri">Rangpuri</option>
                                    <option value="Jasola">Jasola</option>
                                    <option value="Other">Other</option>
                                    -->
                                </select>
                                @if ($errors->has('to_branch'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('to_branch') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Submit
                                </button>
                            </div>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
