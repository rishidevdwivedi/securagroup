<!DOCTYPE html>


<html lang="{{ config('app.locale') }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Styles -->
    <link href="/css/app.css" rel="stylesheet">
    <link href="/css/style.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

    <!-- Scripts -->
    <script>
        window.Laravel = {!! json_encode([
            'csrfToken' => csrf_token(),
        ]) !!};
    </script>
    
    {!! Charts::assets() !!}
</head>
<body>
    <div id="app">
        <nav class="navbar navbar-default navbar-static-top">
            <div class="container">
                <div class="navbar-header">

                    <!-- Collapsed Hamburger -->
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                        <span class="sr-only">Toggle Navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>

                    <!-- Branding Image -->
                    <a class="navbar-brand" href="{{ url('/') }}">
                      <!--  {{ config('app.name', 'Laravel') }} -->
                        <img src="{{URL::asset('/image/Secura-Ex-logo-final.png')}}"  />
                    </a>
                </div>
				<ul class="nav navbar-nav navbar-right">
                        <!-- Authentication Links -->
                        @if (Auth::guest())
                            <li><a href="{{ route('login') }}">Login</a></li>
                            <!--<li><a href="{{ route('register') }}">Register</a></li>-->
                        @else
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                  <strong>Welcome {{ Auth::user()->name }}</strong> <span class="caret"></span>
                                </a>

                                <ul class="dropdown-menu" role="menu">
                                    <li>
                                        <a href="{{ route('logout') }}"
                                            onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                            Logout
                                        </a>

                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                                    </li>
                                </ul>
                            </li>
                        @endif
                    </ul>
                <div class="collapse navbar-collapse" id="app-navbar-collapse">
                    @if (!Auth::guest())
                    <!-- Left Side Of Navbar -->
                    @if(Auth::user()->username == "huma-hub" || Auth::user()->username == "maryam-hub"|| Auth::user()->username == "dolly-hub")
                    <ul class="nav navbar-nav caller">
                            <li><a href="/home">Dashboard</a></li>
                            <li><a href="/caller-consignments">Calling</a></li>
                            <li><a href="/consignments">Consignments</a></li>
                            <li><a href="/tracking/consignment">Track &amp; Trace</a></li>
							 
                                   <li><a href="/new_consignments">Single Day Report</a></li> 
                                 
                    </ul>
                    
					@elseif(Auth::user()->username == "joshi")
					     <ul class="nav navbar-nav caller">
                            <li><a href="/driver/report">View Driver Report</a></li>
                            
                    </ul>
                    @elseif(Auth::user()->name == "jeetendra-hub")
                    <ul class="nav navbar-nav" style="margin-top:26px; margin-left:400px">
                    	<li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                    <strong>{{ 'Operation' }}</strong> <span class="caret"></span>
                                </a>
                                <ul class="dropdown-menu">
                                  <li class="dropdown-header">Step 1 : Scan Consignment</li>
                                  <li><a href="/consignment-verify">In Scan - HUB</a></li>
                                  <li class="divider"></li>
                                  <li class="dropdown-header">Step 2 : Create Bag &amp; Shipments to Bag</li>
                                  <li><a href="/bagging/create">Create Bagging</a></li>
                                  <li class="divider"></li>

                                  <li class="dropdown-header">Step 3 : Move the Bag</li>
                                  <li><a href="/bagging">Transit Bagging</a></li>
                                  <li class="divider"></li>
                                </ul>
                            </li>
                            <li><a href="/consignments">Consignments</a></li>
                    	
                    </ul>
                    @else
                    <ul class="nav navbar-nav">
                            <li><a href="/home">Dashboard</a></li>
							<li><a href="/aging-reports">Ageing</a></li>
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                    {{ 'System' }} <span class="caret"></span>
                                </a>

                                <ul class="dropdown-menu">
                                  <li class="dropdown-header">New Consignment Data</li>
                                  <li><a href="/api/importExport">Import API</a></li>
                                  <li class="divider"></li>
                                  
                                  <li class="dropdown-header">Reverse Consignment Data</li>
                                  <li><a href="/api/reverseImportExport">Reverse Import API</a></li>
                                  <li class="divider"></li>

                                  <li class="dropdown-header">Export Shipment History</li>
                                  <li><a href="/shipment/history">Shipment History</a></li>
                                  <li class="divider"></li>

                                  <li class="dropdown-header">RTO Process</li>
                                  <li><a href="/rto/upload">RTO Upload</a></li>
                                  <li class="divider"></li>

                                  <li class="dropdown-header">Manage Drivers</li>
                                  <li><a href="/drivers/create">Add Driver</a></li>
                                  <li><a href="/drivers">View/Edit Driver</a></li>
                                  <li class="divider"></li>
                                  
								 @if(Auth::user()->name == "Hasan")
                                  <li class="dropdown-header">Branchwise Reports</li>
                                  <li><a href="/branchwise-report">View Report</a></li>
                                  <li class="divider"></li>
                                 @endif 
								 
                                 @if(Auth::user()->name == "Sunil_KR" )
                                  <li class="dropdown-header">Upload Pin Codes</li>
                                  <li><a href="import-pincodes">Upload Pin Codes</a></li>
                                  <li class="divider"></li>
                                 @endif 
                                 
								 @if(Auth::user()->name == "Sunil Kumar" || Auth::user()->name == "Aman Solanki" || Auth::user()->name == "Khushter Imam")
                                  <li class="dropdown-header">Consignments</li>
                                   <li><a href="/new_consignments">Single Day Report</a></li> 
                                  <li class="divider"></li>
                                 @endif 
								 
                                 <li class="dropdown-header">MIS</li>
                                  <li><a href="/mis-download">Download MIS</a></li>
                                  <li class="divider"></li>
                                </ul>
								
								



                            </li>
                            <!--<li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                    {{ 'Manage' }} <span class="caret"></span>
                                </a>

                                <ul class="dropdown-menu">

                                  <li class="dropdown-header">Manage Bags</li>
                                  <li><a href="/bags">Bags</a></li>
                                  <li class="divider"></li>
                                  
                                  <li class="dropdown-header">Manage Pods</li>
                                  <li><a href="/pod">Pods</a></li>
                                  <li class="divider"></li>

                                  <li class="dropdown-header">Manage Customers</li>
                                  <li><a href="/customers">Customers</a></li>
                                  <li class="divider"></li>

                                </ul>
                            </li>-->

                            <!--<li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                    {{ 'Consignments' }} <span class="caret"></span>
                                </a>

                                <ul class="dropdown-menu">
                                  <li><a href="/consignments">Consignments</a></li>
                                   <li class="divider"></li>
                                  <li><a href="/reverse-consignments">Reverse Consignments</a></li>
                                  <li class="divider"></li>
                                </ul>
                            </li>-->
                            <li><a href="/consignments">Consignments</a></li>
                            <li><a href="/bagging">Baggings</a></li>
                            <!--<li><a href="/runsheets">Run Sheets</a></li>-->

                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                    {{ 'Operation' }} <span class="caret"></span>
                                </a>

                                <ul class="dropdown-menu">

                                  <li class="dropdown-header">Step 1 : Scan Consignment</li>
                                  <li><a href="/consignment-verify">In Scan - HUB</a></li>
                                  <li class="divider"></li>

                                  <!--<li class="dropdown-header">Step 2 : Add New Bags (Optional)</li>
                                  <li><a href="/bags/create">Create Bag</a></li>
                                  <li class="divider"></li>-->

                                  <li class="dropdown-header">Step 2 : Create Bag &amp; Shipments to Bag</li>
                                  <li><a href="/bagging/create">Create Bagging</a></li>
                                  <li class="divider"></li>

                                  <li class="dropdown-header">Step 3 : Move the Bag</li>
                                  <li><a href="/bagging">Transit Bagging</a></li>
                                  <li class="divider"></li>
                                  <!--
                                  <li class="dropdown-header">Step 4 : Verify the Bag</li>
                                  <li><a href="/bagging">Verify Bagging</a></li>
                                  <li class="divider"></li>
                                  
                                  <li class="dropdown-header">Step 5 : Scan Consignment</li>
                                  <li><a href="/in-scan">In Scan - BRANCH</a></li>
                                  <li class="divider"></li>

                                  <li class="dropdown-header">Step 6 : Out for Delivery</li>
                                  <li><a href="/out-scan">Out Scan - BRANCH</a></li>
                                  <li class="divider"></li>
                                  
                                  <li class="dropdown-header">Step 7 : Delivery Status</li>
                                  <li><a href="/status">Final Status</a></li>
                                  <li class="divider"></li>
                                  -->
                                  <!--
                                  <li class="dropdown-header">Scan RTO SHIP. & Bag</li>
                                  <li><a href="/bag/verify">RTO Bag Verification</a></li>
                                  <li><a href="/consignment-verification">RTO Consignment Verification</a></li>
                                  <li class="divider"></li>
                                  <li class="dropdown-header">Dispatch RTO Bag</li>
                                  <li><a href="/bagupdate/create">RTO Bag Add</a></li>
                                  <li><a href="/bagupdate/manage">RTO Bag Dispatch</a></li>
                                    -->
                                    
                                </ul>
                            </li>
                            
                           <!--<li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                    {{ 'Rev. Operation' }} <span class="caret"></span>
                                </a>

                                <ul class="dropdown-menu">
                                  <li class="dropdown-header">Step 6 : Verify The Bag</li>
                                  <li><a href="/reverse-bagging">Verify Bagging</a></li>
                                  <li class="divider"></li>
                                  
                                  <li class="dropdown-header">Step 7 : Scan Reverse Consignment</li>
                                  <li><a href="/reverse-in-scan">Receive Shipment At HUB</a></li>
                                  <li class="divider"></li>

                                  <li class="dropdown-header">Step 8 : Manage DTO</li>
                                  <li><a href="/reverse-por/create">Create DTO</a></li>
                                  <li class="divider"></li>
                                  
                                  <li class="dropdown-header">Step 9 : Out Scan To Client</li>
                                  <li><a href="/reverse-out-scan-por">Out Scan To Client</a></li>
                                  <li class="divider"></li>
                                  
                                  <li class="dropdown-header">List</li>
                                  <li><a href="/reverse-porsheets">DTO Sheets</a></li>
                                  <li class="divider"></li>
                                  
                                  <li class="dropdown-header">Step 10 : Reverse Final Status</li>
                                  <li><a href="/reverse-final-status">Reverse Final Status</a></li>
                                  <li class="divider"></li>
                                  
                                </ul>
                                
                            </li>--> 
                           

                          <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                    {{ 'RTO' }} <span class="caret"></span>
                                </a>

                                <ul class="dropdown-menu">
 
                                  <li class="dropdown-header">Step 1 : RTO List / Receive</li>
                                  <li><a href="/rto/listing">RTO List / Receive</a></li>
                                  <li class="divider"></li>

                                  <li class="dropdown-header">Step 2 : RTO Create POD</li>
                                  <li><a href="/pod/create"> RTO Create POD</a></li>
                                  <li class="divider"></li>

                                  <li class="dropdown-header">Step 3 : RTO Out Scan</li>
                                  <li><a href="/out-scan-pod"> RTO Out Scan</a></li>
                                  <li class="divider"></li>

                                  <li class="dropdown-header">List</li>
                                  <li><a href="/podsheets">POD Sheets</a></li>
                                  <li class="divider"></li>

                                  <li class="dropdown-header">Final Status</li>
                                  <li><a href="/rto/final-status">RTO Final Status</a></li>
                                  <li class="divider"></li>

                                </ul>
                            </li>

                            <li><a href="/tracking/consignment">Track & Trace</a></li>
							<li><a href="/shipmentTracking">Shipment Tracking</a></li>
							@if(Auth::user()->name == "Hasan")
                              <li><a href="/audit">Audit</a></li>
						      <li><a href="/new_consignments">Single Day Report</a></li>
							  <li><a href="/branch_sdd_performance">Branch SDD Performance</a></li>
							  <li><a href="/rider_performance">Rider Performance</a></li>
                            @endif 
                            <!--
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                    {{ 'Reports' }} <span class="caret"></span>
                                </a>

                                <ul class="dropdown-menu">
                                  <li><a href="#">Daily Report</a></li>
                                  <li><a href="#">Bag Report</a></li>
                                  <li class="divider"></li>
                                </ul>
                            </li>
                            -->
                    </ul>
                    
                    @endif 
                    
                    @endif
                    <!-- Right Side Of Navbar -->
                    
                </div>
            </div>
        </nav>

        @yield('content')
    </div>

    <!-- Scripts -->
    <script src="/js/app.js"></script>
</body>
</html>
