@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Add New Consignment</div>
                <div class="panel-body">
                    <form class="form-horizontal" role="form" method="POST" action="/consignments">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('data_received_date') ? ' has-error' : '' }}">
                            <label for="data_received_date" class="col-md-4 control-label">Data Received Date</label>

                            <div class="col-md-6">
                                <input id="data_received_date" type="date" class="form-control" name="data_received_date" value="{{ old('data_received_date') }}" required autofocus>

                                @if ($errors->has('data_received_date'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('data_received_date') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('awb') ? ' has-error' : '' }}">
                            <label for="awb" class="col-md-4 control-label">AWB</label>

                            <div class="col-md-6">
                                <input id="awb" type="text" class="form-control" name="awb" value="{{ old('awb') }}" required autofocus>

                                @if ($errors->has('awb'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('awb') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('order_id') ? ' has-error' : '' }}">
                            <label for="order_id" class="col-md-4 control-label">Order ID</label>

                            <div class="col-md-6">
                                <input id="order_id" type="text" class="form-control" name="order_id" value="{{ old('order_id') }}" required autofocus>

                                @if ($errors->has('order_id'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('order_id') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>


                        <div class="form-group{{ $errors->has('customer_name') ? ' has-error' : '' }}">
                            <label for="customer_name" class="col-md-4 control-label">Customer Name</label>

                            <div class="col-md-6">
                                <input id="customer_name" type="text" class="form-control" name="customer_name" value="{{ old('customer_name') }}" required autofocus>

                                @if ($errors->has('customer_name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('customer_name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>


                        <div class="form-group{{ $errors->has('collectable_value') ? ' has-error' : '' }}">
                            <label for="collectable_value" class="col-md-4 control-label">Collectable Value</label>

                            <div class="col-md-6">
                                <input id="collectable_value" type="number" class="form-control" name="collectable_value" value="{{ old('collectable_value') }}" required autofocus>

                                @if ($errors->has('collectable_value'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('collectable_value') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('item_description') ? ' has-error' : '' }}">
                            <label for="item_description" class="col-md-4 control-label">Item Description</label>

                            <div class="col-md-6">
                                <textarea class="form-control" name="item_description"  required autofocus>{{ old('item_description') }}</textarea>

                                @if ($errors->has('item_description'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('item_description') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('consignee') ? ' has-error' : '' }}">
                            <label for="consignee" class="col-md-4 control-label">Consignee</label>

                            <div class="col-md-6">
                                <textarea class="form-control" name="item_description"  required autofocus>{{ old('consignee') }}</textarea>

                                @if ($errors->has('consignee'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('consignee') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('consignee_address') ? ' has-error' : '' }}">
                            <label for="consignee_address" class="col-md-4 control-label">Consignee Address</label>

                            <div class="col-md-6">
                                <textarea class="form-control" name="consignee_address"  required autofocus>{{ old('consignee_address') }}</textarea>

                                @if ($errors->has('consignee_address'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('consignee_address') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>


                        <div class="form-group{{ $errors->has('destination_city') ? ' has-error' : '' }}">
                            <label for="destination_city" class="col-md-4 control-label">Destination City</label>

                            <div class="col-md-6">
                                <input id="destination_city" type="text" class="form-control" name="destination_city" value="{{ old('destination_city') }}" required autofocus>

                                @if ($errors->has('destination_city'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('destination_city') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>


                        <div class="form-group{{ $errors->has('pincode') ? ' has-error' : '' }}">
                            <label for="pincode" class="col-md-4 control-label">Pincode</label>

                            <div class="col-md-6">
                                <input id="pincode" type="text" class="form-control" name="pincode" value="{{ old('pincode') }}" required autofocus>

                                @if ($errors->has('pincode'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('pincode') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('state') ? ' has-error' : '' }}">
                            <label for="state" class="col-md-4 control-label">State</label>

                            <div class="col-md-6">
                                <input id="state" type="text" class="form-control" name="state" value="{{ old('state') }}" required autofocus>

                                @if ($errors->has('state'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('state') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>


                        <div class="form-group{{ $errors->has('mobile') ? ' has-error' : '' }}">
                            <label for="mobile" class="col-md-4 control-label">Mobile</label>

                            <div class="col-md-6">
                                <input id="mobile" type="text" class="form-control" name="mobile" value="{{ old('mobile') }}" required autofocus>

                                @if ($errors->has('mobile'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('mobile') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>


                        <div class="form-group{{ $errors->has('current_status') ? ' has-error' : '' }}">
                            <label for="current_status" class="col-md-4 control-label">Current Status</label>

                            <div class="col-md-6">
                                <input id="current_status" type="text" class="form-control" name="current_status" value="{{ old('current_status') }}" required autofocus>

                                @if ($errors->has('current_status'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('current_status') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('remarks') ? ' has-error' : '' }}">
                            <label for="remarks" class="col-md-4 control-label">Remarks</label>

                            <div class="col-md-6">
                                <input id="remarks" type="text" class="form-control" name="remarks" value="{{ old('remarks') }}" required autofocus>

                                @if ($errors->has('remarks'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('remarks') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('prev_status') ? ' has-error' : '' }}">
                            <label for="prev_status" class="col-md-4 control-label">Previous Status</label>

                            <div class="col-md-6">
                                <input id="prev_status" type="text" class="form-control" name="prev_status" value="{{ old('prev_status') }}" required autofocus>

                                @if ($errors->has('prev_status'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('prev_status') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>


                        <div class="form-group{{ $errors->has('prev_sub_status') ? ' has-error' : '' }}">
                            <label for="prev_sub_status" class="col-md-4 control-label">Previous Sub Status</label>

                            <div class="col-md-6">
                                <input id="prev_sub_status" type="text" class="form-control" name="prev_sub_status" value="{{ old('prev_sub_status') }}" required autofocus>

                                @if ($errors->has('prev_sub_status'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('prev_sub_status') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('no_of_attempts') ? ' has-error' : '' }}">
                            <label for="no_of_attempts" class="col-md-4 control-label">No of Attempts</label>

                            <div class="col-md-6">
                                <input id="no_of_attempts" type="number" class="form-control" name="no_of_attempts" value="{{ old('no_of_attempts') }}" required autofocus>

                                @if ($errors->has('no_of_attempts'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('no_of_attempts') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                         <div class="form-group{{ $errors->has('last_updated_on') ? ' has-error' : '' }}">
                            <label for="last_updated_on" class="col-md-4 control-label">Last Updated_On</label>

                            <div class="col-md-6">
                                <input id="last_updated_on" type="date" class="form-control" name="last_updated_on" value="{{ old('last_updated_on') }}" required autofocus>

                                @if ($errors->has('last_updated_on'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('last_updated_on') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>


                        <div class="form-group{{ $errors->has('last_updated_by') ? ' has-error' : '' }}">
                            <label for="last_updated_by" class="col-md-4 control-label">Last Updated_By</label>

                            <div class="col-md-6">
                                <input id="last_updated_by" type="text" class="form-control" name="last_updated_by" value="{{ old('last_updated_by') }}" required autofocus>

                                @if ($errors->has('last_updated_by'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('last_updated_by') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('bag_code') ? ' has-error' : '' }}">
                            <label for="bag_code" class="col-md-4 control-label">Bag Code</label>

                            <div class="col-md-6">
                                <input id="bag_code" type="text" class="form-control" name="bag_code" value="{{ old('prev_sub_status') }}" required autofocus>

                                @if ($errors->has('bag_code'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('bag_code') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>


                        <div class="form-group{{ $errors->has('drs_code') ? ' has-error' : '' }}">
                            <label for="drs_code" class="col-md-4 control-label">Drs Code</label>

                            <div class="col-md-6">
                                <input id="drs_code" type="text" class="form-control" name="drs_code" value="{{ old('drs_code') }}" required autofocus>

                                @if ($errors->has('drs_code'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('drs_code') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Submit
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
