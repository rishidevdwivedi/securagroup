@extends('layouts.app')

@section('content')
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>

    <link href="/css/home.css" rel="stylesheet">
    
    



<div class="container">
<div class="row" style="margin:20px 0">

</div>
            


<div class="row" style="margin-top:10px; margin-bottom:10px">
<script type="text/javascript">
	  google.charts.load('current', {'packages':['corechart']});


      // Set a callback to run when the Google Visualization API is loaded.
	  google.setOnLoadCallback(shipmentAging);
	  google.setOnLoadCallback(shipmentAgingRTO);
	  
      function shipmentAging() {

        var data = new google.visualization.DataTable();
        data.addColumn('string', 'Topping');
        data.addColumn('number', 'Slices');
        data.addRows([
          ['Two Days(Delhi NCR)', <?php echo $twoDaysAgedDelhiNcr; ?>],
          ['More Than 2 Days(Delhi NCR)', <?php echo $moreDaysAgedDelhiNcr; ?>],
		  ['Three Days(Out Of Delhi)', <?php echo $threeDaysAgedOutsideDelhi; ?>],
		  ['More Than 3 Days(Out Of Delhi)', <?php echo $moreDaysAgedOutsideDelhi; ?>]
        ]);

        var options = {'title':'Shipment Ageing',
                       'width':570,
                       'height':350,
					    pieHole: 0.4,
						pieSliceTextStyle: {color: 'white', bold: true},
						chartArea:{left:10,right:10,top:50,width:'100%',height:'70%'},
						legend:{position: 'bottom'},
					   'pieSliceText':'value'};

        var chart = new google.visualization.PieChart(document.getElementById('chart_div_shipmentAging'));
        chart.draw(data, options);

        google.visualization.events.addListener(chart, 'select', selectHandler);

        var label1 = data.pieSliceText('label');

function selectHandler() {
    var selection = chart.getSelection();
    if (selection.length) {
        var pieSliceLabel = data.getValue(selection[0].row, 0);
		window.location.href = '/aging-reports/exportExcel?type=aging&agingDuration=' + pieSliceLabel;
    }
}

      }
	  
	  
      function shipmentAgingRTO() {

        var data = new google.visualization.DataTable();
        data.addColumn('string', 'Topping');
        data.addColumn('number', 'Slices');
        data.addRows([
		<?php	
		foreach($clients as $key => $value)
		{
         echo "['".$key." - Two Days', ".$value['twoDaysAged']." ],";
		 echo "['".$key." - More Than 2 Days', ".$value['moreDaysAged']." ],";
		}
		?>  
        ]);

        var options = {'title':'Shipment Ageing Clientwise',
                       'width':570,
                       'height':350,
					    pieHole: 0.4,
						pieSliceTextStyle: {color: 'white', bold: true},
						chartArea:{left:10,right:10,top:50,width:'100%',height:'70%'},
						legend:{position: 'bottom'},
					   'pieSliceText':'value'};

        var chart = new google.visualization.PieChart(document.getElementById('chart_div_shipmentAgingRTO'));
        chart.draw(data, options);

        google.visualization.events.addListener(chart, 'select', selectHandler);

        var label1 = data.pieSliceText('label');

function selectHandler() {
    var selection = chart.getSelection();
    if (selection.length) {
        var pieSliceLabel = data.getValue(selection[0].row, 0);
		window.location.href = '/aging-reports/exportExcel?type=clientWiseAging&agingDuration=' + pieSliceLabel;
    }
}

      }
	  
    </script>
   <!-- /.row -->
         <div class="col-lg-6 col-md-6" style="border-right:1px dotted #333">
            <div id="chart_div_shipmentAging"></div>
         </div>
         <div class="col-lg-6 col-md-6">
            <div id="chart_div_shipmentAgingRTO"></div>
         </div>
   <!-- /.col-lg-4 -->
</div>
</div>
@endsection
