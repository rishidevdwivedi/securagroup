@extends('layouts.app')

@section('content')

    <link href="/css/home.css" rel="stylesheet">


<div class="container">
<div class="row">
<form method="GET" action="/home" >
   <div class="form-group">
      <label for="datepicker_from" class="col-md-1 control-label">Created From Date:</label>
      <div class="col-md-2">
         <input class="form-control" type="text" name="date_from" id="datepicker_from" placeholder="yyyy-mm-dd">
      </div>
   </div>

   <div class="form-group">
      <label for="datepicker_to" class="col-md-1 control-label">Created To Date:</label>
      <div class="col-md-2">
         <input class="form-control" type="text" name="date_to" id="datepicker_to" placeholder="yyyy-mm-dd">
      </div>
   </div>

   <!--
   <div class="form-group">
      <label for="to_branch" class="col-md-1 control-label">Sort By Branch:</label>
      <div class="col-md-2">
         <select id="to_branch" class="form-control" name="branch"   autofocus>
            <option value="">None</option>
            <option value="Ghaziabad" @if(isset($_GET['branch']) &&  ($_GET['branch'] == 'Ghaziabad')) selected  @endif >Ghaziabad</option>
            <option value="Rangpuri" @if(isset($_GET['branch']) &&  ($_GET['branch'] == 'Rangpuri')) selected  @endif >Rangpuri</option>
            <option value="Jasola" @if(isset($_GET['branch']) &&  ($_GET['branch'] == 'Jasola')) selected  @endif >Jasola</option>
            <option value="ODA" @if(isset($_GET['branch']) &&  ($_GET['branch'] == 'ODA')) selected  @endif >ODA</option>
         </select>
      </div>
   </div>
   <div class="form-group">
      <label for="customer_name" class="col-md-1 control-label">Sort By Customer:</label>
      <div class="col-md-2">
         <select id="customer_name" class="form-control" name="customer_name"   autofocus>
            <option value="">None</option>
            <option value="SW" @if(isset($_GET['customer_name']) &&  ($_GET['customer_name'] == 'SW')) selected  @endif >SW</option>
         </select>
      </div>
   </div>
   -->
   <button type="submit" class="btn btn-sm btn-primary btn-create">Filter</button>
</form>
</div>
<br /><br />

<div class="row">
   <!-- /.row -->
   <div class="col-sm-12">
      <div class="row">
         <div class="col-lg-3 col-md-6">
            <div class="panel panel-primary">
               <div class="panel-heading">
                  <div class="row">
                     <div class="col-xs-3">
                        <i class="fa fa-truck fa-5x"></i>
                     </div>
                     <div class="col-xs-9 text-right">
                        <div class="huge">{{ $intransit }}</div>
                        <div>Intransit!</div>
                     </div>
                  </div>
               </div>

               <a href="/home/exportExcel?date_from=<?php if(isset($_GET['date_from'])) { echo  $_GET['date_from']; } ?>&date_to=<?php if(isset($_GET['date_to'])) { echo  $_GET['date_to']; } ?>&type=intransit">
                  <div class="panel-footer">
                     <span class="pull-left">Download Details</span>
                     <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                     <div class="clearfix"></div>
                  </div>
               </a>
            </div>
         </div>
         <div class="col-lg-3 col-md-6">
            <div class="panel panel-green">
               <div class="panel-heading">
                  <div class="row">
                     <div class="col-xs-3">
                        <i class="fa fa-tasks fa-5x"></i>
                     </div>
                     <div class="col-xs-9 text-right">
                        <div class="huge">{{ $delivered }}</div>
                        <div>Delivered!</div>
                     </div>
                  </div>
               </div>
               <a href="/home/exportExcel?date_from=<?php if(isset($_GET['date_from'])) { echo  $_GET['date_from']; } ?>&date_to=<?php if(isset($_GET['date_to'])) { echo  $_GET['date_to']; } ?>&type=delivered">
                  <div class="panel-footer">
                     <span class="pull-left">Download Details</span>
                     <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                     <div class="clearfix"></div>
                  </div>
               </a>
            </div>
         </div>
         <div class="col-lg-3 col-md-6">
            <div class="panel panel-yellow">
               <div class="panel-heading">
                  <div class="row">
                     <div class="col-xs-3">
                        <i class="fa fa-money fa-5x"></i>
                     </div>
                     <div class="col-xs-9 text-right">
                        <div class="huge">{{ $cod }}</div>
                        <div>COD!</div>
                     </div>
                  </div>
               </div>
               <a href="/home/exportExcel?date_from=<?php if(isset($_GET['date_from'])) { echo  $_GET['date_from']; } ?>&date_to=<?php if(isset($_GET['date_to'])) { echo  $_GET['date_to']; } ?>&type=cod">
                  <div class="panel-footer">
                     <span class="pull-left">Download Details</span>
                     <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                     <div class="clearfix"></div>
                  </div>
               </a>
            </div>
         </div>
         <div class="col-lg-3 col-md-6">
            <div class="panel panel-red">
               <div class="panel-heading">
                  <div class="row">
                     <div class="col-xs-3">
                        <i class="fa fa-support fa-5x"></i>
                     </div>
                     <div class="col-xs-9 text-right">
                        <div class="huge">{{ $undelivered }}</div>
                        <div>UnDelivered!</div>
                     </div>
                  </div>
               </div>
               <a href="/home/exportExcel?date_from=<?php if(isset($_GET['date_from'])) { echo  $_GET['date_from']; } ?>&date_to=<?php if(isset($_GET['date_to'])) { echo  $_GET['date_to']; } ?>&type=undelivered">
                  <div class="panel-footer">
                     <span class="pull-left">Download Details</span>
                     <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                     <div class="clearfix"></div>
                  </div>
               </a>
            </div>
         </div>
      </div>

   </div>
   <!-- /.col-lg-4 -->
</div>


<div class="row">
   <!-- /.row -->
   <div class="col-sm-12">
      <div class="row">
         <div class="col-lg-3 col-md-6">
            <div class="panel panel-primary">
               <div class="panel-heading">
                  <div class="row">
                     <div class="col-xs-3">
                        <i class="fa fa-retweet fa-5x"></i>
                     </div>
                     <div class="col-xs-9 text-right">
                        <div class="huge">{{ $rto }}</div>
                        <div>RTO!</div>
                     </div>
                  </div>
               </div>
               <a href="/home/exportExcel?date_from=<?php if(isset($_GET['date_from'])) { echo  $_GET['date_from']; } ?>&date_to=<?php if(isset($_GET['date_to'])) { echo  $_GET['date_to']; } ?>&type=rto">
                  <div class="panel-footer">
                     <span class="pull-left">Download Details</span>
                     <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                     <div class="clearfix"></div>
                  </div>
               </a>
            </div>
         </div>
         <div class="col-lg-3 col-md-6">
         </div>
         <div class="col-lg-3 col-md-6">
         </div>
         <div class="col-lg-3 col-md-6">
         </div>
      </div>
      
      <div class="row">
      <h3>Aged Consignments</h3>
         <div class="col-lg-6 col-md-6">
          <div class="app">
            <center>
                {!! $regularPie->render() !!}
            </center>
        </div>
        <div class="chart_div">
          <a href="consignments-aging" class="btn btn-primary" role="button">Show Details</a>
        </div>
        <!-- End Of Main Application -->
        
         </div>
         
         <div class="col-lg-6 col-md-6">
         
          <div class="app">
            <center>
                {!! $rtoPie->render() !!}
            </center>
        </div>
        <div class="chart_div">
        
          <a href="rto-consignments-aging" class="btn btn-primary" role="button">Show Details</a>
        </div>
        <!-- End Of Main Application -->
        
         </div>
      </div>   

   </div>
   <!-- /.col-lg-4 -->
</div>


</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>


  <script>
$.noConflict();  //Not to conflict with other scripts
jQuery(document).ready(function($) {


      var getUrlParameter = function getUrlParameter(sParam) {
      var sPageURL = decodeURIComponent(window.location.search.substring(1)),
          sURLVariables = sPageURL.split('&'),
          sParameterName,
          i;

      for (i = 0; i < sURLVariables.length; i++) {
          sParameterName = sURLVariables[i].split('=');

          if (sParameterName[0] === sParam) {
              return sParameterName[1] === undefined ? true : sParameterName[1];
          }
      }
  };

if(getUrlParameter('date_from')){
var date_from = getUrlParameter('date_from');

}else{
var date_from = new Date();
}

if(getUrlParameter('date_to')){
var date_to = getUrlParameter('date_to');

}else{
var date_to = new Date();
}


    $( "#datepicker_from" ).datepicker({
        changeMonth: true,
        changeYear: true,
        dateFormat : 'yy-mm-dd',
        defaultDate: new Date(),
    }).datepicker("setDate", date_from);

    $( "#datepicker_to" ).datepicker({
        changeMonth: true,
        changeYear: true,
        dateFormat : 'yy-mm-dd',
        defaultDate: new Date(),
    }).datepicker("setDate", date_to);


  } );
  </script>



@endsection
