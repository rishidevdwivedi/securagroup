@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">
                   
                    Edit Bagging Status 

                </div>

                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-8 col-md-offset-2">
                            Bag Code : {{ $bag->bag_code }}
                        </div>

                        <div class="col-md-8 col-md-offset-2">
                            For Branch  : {{ $bag->to_branch }}
                        </div>
                    </div>
                    Consignments
                    <div class="row">
                        <div class="col-md-8 col-md-offset-2">
                        @foreach($bag->bag_consignments as $consignment)                        
                                {{ $consignment->awb }}
                        @endforeach
                        </div>
                    </div>
<hr>
                     <form class="form-horizontal"  method="POST" action="/bagging/updateStatus/{{ $bag->id }}">
                        {{ csrf_field() }}
                        {{ method_field('PUT') }}


                        <div class="form-group{{ $errors->has('status') ? ' has-error' : '' }}">
                            <label for="status" class="col-md-4 control-label">Status</label>

                            <div class="col-md-4">
                                <select id="status" class="form-control" name="status"  required autofocus>
                                    <option value="">Choose Bag Status</option>
                                    <option value="0" @if(!$bag->in_transit) selected  @endif  >Processing</option>
                                    <option value="1" @if($bag->in_transit) selected  @endif  >In Transit</option>
                                </select>
                                @if ($errors->has('status'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('status') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Update
                                </button>
                            </div>
                        </div>

                    </form>




                </div>



            </div>
        </div>
    </div>
</div>
@endsection
