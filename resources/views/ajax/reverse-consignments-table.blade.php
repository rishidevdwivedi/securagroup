

        <div class="col-md-11 col-md-offset-1">



            <h4>Toatal Records Found : {{ $consignments->total() }} </h4>



            <div class="panel panel-default panel-table">

              <div class="panel-heading">

                <div class="row">

                  <div class="col col-xs-6">

                    <h3 class="panel-title">Consignments</h3>

                  </div>

                  <div class="col col-xs-6 text-right">

                    <a href="/reverse-consignments/create"><button type="button" class="btn btn-sm btn-primary btn-create">Add New</button></a>

                    <a href="/reverse-consignments/reverseExportExcel?date=<?php if(isset($_GET['date'])) { echo  $_GET['date']; } ?>&branch=<?php if(isset($_GET['branch'])) { echo  $_GET['branch']; } ?>&customer_name=<?php if(isset($_GET['customer_name'])) { echo  $_GET['customer_name']; } ?>&field=<?php if(isset($field)) { echo $field; } ?>&value=<?php if(isset($value)) { echo $value; } ?>"><button type="button" class="btn btn-sm btn-primary btn-create">Export</button></a>

                  </div>







                </div>

              </div>

              <div class="panel-body">

                



                    <table class="table table-striped table-bordered table-list">

                      <thead>

                        <tr>

                            <th><em class="fa fa-cog"></em></th>

                            <!--<th class="hidden-xs">ID</th>-->

                            <th>Data Received Date</th>
                            <th>AWB</th>
                            <th>Customer Name</th>
                            <th>Collectable Value</th>
                     		<th>Delivery Boy</th>
                            <th>Pincode</th>  
                            <th>Last Updated BY</th>     
                            <th>Last Updated AT</th>
                            <th>Current Status</th>
                            <th>Branch</th> 
                            <th>Attempts</th>
                        </tr> 

                      </thead>

                <!--<?php $action = false; $rto_count = 0; $i=0; ?>-->

                @forelse($consignments as $consignment)

                      <!--

                      <?php if(empty($consignment->rto_reason)) $action=true; else {$rto_count = $rto_count+1 ;} ?>

                      -->

                      <tbody>  

                              <tr>

                                <td align="center">

                                  <a target="_blank" class="btn btn-default" href="/reverse-consignments/{{ $consignment->id }}" ><em class="fa fa-eye"></em></a>                                  

                                  <a class="btn btn-default" href="/reverse-consignments/{{ $consignment->id }}/edit" ><em class="fa fa-pencil"></em></a>

                                  <?php if(\Config::get('constants.deliveryStatus3') == $consignment->current_status){ ?>

                                  <form action="/consignments/repeat/{{ $consignment->id }}" method="POST" class="pull-right" onsubmit="return confirm('Are you sure you want to repeat this consignment?')" >{{ csrf_field() }} <button type="submit" class="btn btn-danger"><em class="fa fa-repeat" ></em></button></form>                            

                                  <?php } ?>

                                  <!--<form action="/consignments/{{ $consignment->id }}" method="POST" class="pull-right" onsubmit="return confirm('Are you sure you want to delete?')" >{{ csrf_field() }} {{ method_field('DELETE') }}<button type="submit" class="btn btn-danger"><em class="fa fa-trash" ></em></button></form>-->

                                </td>

                                <!--<td class="hidden-xs">{{ $consignment->id }}</td>-->

                                <td>{{ $consignment->data_received_date }}</td>
                                <td>{{ $consignment->awb }}</td>
                                <td>{{ $consignment->customer_name }}</td> 
                     			<td>{{ $consignment->collectable_value }}</td>                     
                    			<td>{{ $dev_boy[$i] }}</td> 
                                <td>{{ $consignment->pincode }}</td>  
                                <td>{{ $consignment->last_updated_by }}</td>                            
                                <td>{{ $consignment->updated_at->format('Y-m-d g:i A') }}</td>                            
                                <td>{{ $consignment->current_status }}</td>
                                <td>{{ $consignment->branch }}</td>
                                <td>{{ $consignment->no_of_attempts }}</td>                                                                                            

                              </tr>



                      </tbody>

                      <?php $i = $i+1; ?>

                @empty

                    No Consignments.



                @endforelse



                    </table>

                <!--<?php echo "RTO Found : " . $rto_count; ?>-->



              

              </div>

              <div class="panel-footer">



                <div class="row">

                  

                  <div class="col col-xs-4">Page {{ $consignments->currentPage() }} of {{ $consignments->lastPage()  }}

                  </div>

                  <div class="col col-xs-8 pull-right">

                    <ul class="pagination hidden-xs pull-right">



                       {{ $consignments->appends(request()->input())->links() }} 



                    </ul>



                  </div>

                </div>

              </div>

            </div>

            </div>