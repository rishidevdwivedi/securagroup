        <div class="col-md-11 col-md-offset-1">

            <h4>Toatal Records Found : {{ $consignments->total() }} </h4>

            <div class="panel panel-default panel-table">
              <div class="panel-heading">
                <div class="row">
                  <div class="col col-xs-6">
                    <h3 class="panel-title">Consignments</h3>
                  </div>
                  <div class="col col-xs-6 text-right">
                  </div>
                </div>
              </div>
              <div class="panel-body">
                
					@if($consignments->contains('current_status', \Config::get('constants.rtoIntransittoHub')))
                         <form action="/rto/receive-multiple" method="POST"  
onsubmit="return confirm('Are you sure you want to repeat these outside consignments?')" >{{ csrf_field() }}
					@endif
                    <table class="table table-striped table-bordered table-list">
                      <thead>
                        <tr>
                            <th>@if($consignments->contains('current_status', \Config::get('constants.rtoIntransittoHub')))
                            	  &nbsp;&nbsp;&nbsp;&nbsp;<input type="checkbox" id="checkAll" >	
                                @else 
                                   <em class="fa fa-cog"></em>
                                @endif
                            </th>
                            <th>AWB</th>
                     		<th>Rcvd. From Branch</th>
                            <th>Current Status</th>     
                            <th>Created Date</th>
                            <th>Updated Date</th>
                        </tr> 
                      </thead>
					
                @forelse($consignments as $consignment)

                      <tbody>  
                              <tr>
                                <td align="center">
                                  <!--<a class="btn btn-default" href="/consignments/{{ $consignment->id }}" ><em class="fa fa-eye"></em></a>-->
                                  <?php //if(\Config::get('constants.deliveryStatus3') == $consignment->current_status){ ?>
                                   <!--<form action="/outside-consignments/repeatSingle/{{ $consignment->id }}" method="POST" class="pull-right" 
onsubmit="return confirm('Are you sure you want to repeat this outside consignment?')" >{{ csrf_field() }} <button type="submit" class="btn btn-danger"><em class="fa fa-repeat" ></em></button></form>-->
                                  <?php //} ?>                                  
                                  <!--<a class="btn btn-default" href="/consignments/{{ $consignment->id }}/edit" ><em class="fa fa-pencil"></em></a>-->
                                  
                                  <?php if(\Config::get('constants.rtoIntransittoHub') == $consignment->current_status){ ?>
                                   <input type="checkbox" name="repeat[]" value="<?php echo $consignment->awb ?>">
                                   
                                  <?php } ?>
                                </td>
                                <td>{{ $consignment->awb }}</td>
                    			<td>{{ $consignment->branch }}</td> 
                                <td>{{ $consignment->current_status }}</td>                            
                                <td>{{ $consignment->created_at->format('Y-m-d g:i A') }}</td>                            
                                <td>{{ $consignment->updated_at->format('Y-m-d g:i A') }}</td> 
                              </tr>

                      </tbody>
                @empty
                    No Consignments.

                @endforelse

                    </table>

               @if($consignments->contains('current_status', \Config::get('constants.rtoIntransittoHub')))
              <input type="submit" name="formSubmit" class="btn btn-danger" value="Receive" /></form>
              @endif
              </div>
              <div class="panel-footer">

                <div class="row">
                  
                  <div class="col col-xs-4">Page {{ $consignments->currentPage() }} of {{ $consignments->lastPage()  }}
                  </div>
                  <div class="col col-xs-8 pull-right">
                    <ul class="pagination hidden-xs pull-right">
                        {{ $consignments->links() }}
                    </ul>

                  </div>
                </div>
              </div>
            </div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

<script>
$.noConflict();  //Not to conflict with other scripts
jQuery(document).ready(function($) {            
            $('#checkAll').click(function () {    
     $('input:checkbox').prop('checked', this.checked);    
 });
			
});
</script>
<script>
    var timer;
    function up() {
        timer = setTimeout(function() { 
            var keywords = $('#search_box').val(); 
            /*var param = $('#search_param').val();*/ 
			//alert(keywords);
            if (keywords.length > 0) { 
              $("#loading").show();
              $.ajax({
                  url: '/ajax-search-rto-consignments',
                  method: 'GET',
                  data:{keywords: keywords}
              }).done(function(response){
                  $("#loading").hide();
                  $('#search-results').html(response);          
              });
            }
        }, 500);
    }

    function down() {
        clearTimeout(timer);
    }
</script>			