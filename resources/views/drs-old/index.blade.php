@extends('layouts.app')

@section('content')
<div class="container-fluid">

    <div class="row">
      <div class="col-xs-8 col-xs-offset-4">
         <img id="loading" src="{{URL::asset('/image/loading-dash.gif')}}" style="display:none" alt="Loading..." height="200" width="250">
      </div>
    </div>

    <div class="row"  id="search-results">

        <div class="col-md-11 col-md-offset-1">

            <h4>Toatal Records Found : {{ $drs->total() }} </h4>


            <div class="panel panel-default panel-table">
              <div class="panel-heading">
                <div class="row">
                  <div class="col col-xs-6">
                    <h3 class="panel-title">Drs</h3>
                  </div>
                  <div class="col col-xs-6 text-right">
                    <a href="/drs/create"><button type="button" class="btn btn-sm btn-primary btn-create">Add New</button></a>
                  </div>



                </div>
              </div>
              <div class="panel-body">
                

                    <table class="table table-striped table-bordered table-list">
                      <thead>
                        <tr>
                            <th><em class="fa fa-cog"></em></th>
                            <!--<th class="hidden-xs">ID</th>-->
                            <th>Drs Code</th>
                            <th>Delivery Boy</th>
                            <th>Delivery Date</th>                            
                            <th>Created Date</th>     
                            <th>Updated Date</th>                                                                        
                        </tr> 
                      </thead>

                @forelse($drs as $dr)

                      <tbody>  
                              <tr>
                                <td align="center">
                                  <a class="btn btn-default" href="/drs/{{ $dr->id }}/edit" ><em class="fa fa-pencil"></em></a>
                                  <form action="/drs/{{ $dr->id }}" method="POST" class="pull-right" onsubmit="return confirm('Are you sure you want to delete?')" >{{ csrf_field() }} {{ method_field('DELETE') }}<button type="submit" class="btn btn-danger"><em class="fa fa-trash" ></em></button></form>
                                </td>
                                <td>{{ $dr->drs_code }}</td>
                                <td>{{ $dr->delivery_boy }}</td>
                                <td>{{ $dr->delivery_date }}</td>                                
                                <td>{{ $dr->created_at->format('Y-m-d g:i A') }}</td>                            
                                <td>{{ $dr->updated_at->format('Y-m-d g:i A') }}</td>                            
                              </tr>

                      </tbody>
                @empty
                    No bags.

                @endforelse

                    </table>

              
              </div>
              <div class="panel-footer">

                <div class="row">
                  
                  <div class="col col-xs-4">Page {{ $drs->currentPage() }} of {{ $drs->lastPage()  }}
                  </div>
                  <div class="col col-xs-8 pull-right">
                    <ul class="pagination hidden-xs pull-right">

                       {{ $drs->appends(request()->input())->links() }} 

                    </ul>

                  </div>
                </div>
              </div>
            </div>

    </div>
</div>


@endsection
