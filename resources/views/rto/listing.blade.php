@extends('layouts.app')



@section('content')

<div class="container-fluid">



<div class="row">

      <div class="col-xs-12 col-xs-offset-3">

        

           <div class="form-group">

              <label for="search_box" class="col-md-2 control-label text-right">Search AWB:</label>

              <div class="col-md-3">

                 <!--<input class="form-control" type="text" name="date" id="datepicker" placeholder="yyyy-mm-dd">-->
                 <textarea id="search_box" class="form-control" name="x"  class="form-control" placeholder="Enter multiple awbs to be searched.."  onkeyup="up()" onkeydown="down()" autofocus></textarea>

              </div>

           </div>
           
          


      </div>

    </div>
    
<div class="row">
    <div class="col-xs-10 col-xs-offset-1">
        <div class="strike">
        	<span><strong>OR</strong></span>
        </div>
    </div>
</div>


    <div class="row">

      <div class="col-xs-12 col-xs-offset-1">

        <form method="GET" action="/rto/listing" >

           <div class="form-group">

              <label for="to_branch" class="col-md-2 control-label">Created Date:</label>

              <div class="col-md-2">

                 <input class="form-control" type="text" name="date" id="datepicker" placeholder="yyyy-mm-dd">

              </div>

           </div>
           <div class="col-md-4">
           <div class="form-group">
            <label for="to_branch" class="col-md-6 control-label">Sort By Branch:</label>
            <div class="col-md-6">
               <select id="to_branch" class="form-control" name="branch"   autofocus>
                  <option value="">None</option>
                  @foreach($branches as $branch)
                  <option value="{{ ucfirst($branch->username) }}" @if(isset($_GET['branch']) &&  ($_GET['branch'] == ucfirst($branch->username))) selected  @endif>{{ ucfirst($branch->username) }}</option>
                  @endforeach
               </select>
            </div>
         </div>
</div>
           <button type="submit" class="btn btn-sm btn-primary btn-create">Filter</button>

        </form>

      </div>

    </div>





    <div class="row">

      <div class="col-xs-8 col-xs-offset-4">

         <img id="loading" src="{{URL::asset('/image/loading-dash.gif')}}" style="display:none" alt="Loading..." height="200" width="250">

      </div>

    </div>



    <div class="row"  id="search-results">



        <div class="col-md-11 col-md-offset-1">



            <h4>Toatal Records Found : {{ $rto_consignments->total() }} </h4>





            <div class="panel panel-default panel-table">

              <div class="panel-heading">

                <div class="row">

                  <div class="col col-xs-6">

                    <h3 class="panel-title">RTO</h3>

                  </div>

                  <!--

                  <div class="col col-xs-6 text-right">

                    <a href="/bags/create"><button type="button" class="btn btn-sm btn-primary btn-create">Add New</button></a>

                  </div>

              	  -->







                </div>

              </div>

              <div class="panel-body">

                



                    <table class="table table-striped table-bordered table-list">

                      <thead>

                        <tr>

                            <th><em class="fa fa-cog"></em></th>

                            <!--<th class="hidden-xs">ID</th>-->

                            <th>AWB</th>
                            
                            <th>Rcvd. From Branch</th>

                            <th>Current Status</th>

                            <th>Created Date</th>     

                            <th>Updated Date</th>                                                                        

                        </tr> 

                      </thead>



                @forelse($rto_consignments as $rto)



                      <tbody>  

                              <tr>

                                <td align="center">

                                  <a  target="_blank" class="btn btn-default" href="/consignments/{{ $rto->id }}" ><em class="fa fa-eye"></em></a>                                  

                                  <!--

                                  <form action="/rto/complete/{{ $rto->id }}" method="POST" class="pull-right" onsubmit="return confirm('Are you sure you want to confirm that this RTO has been completed at HUB?')" >{{ csrf_field() }} {{ method_field('POST') }}<button type="submit" class="btn btn-danger"><em class="fa fa-check-circle" ></em></button></form>

                                -->

                                  <?php if(\Config::get('constants.rtoIntransittoHub') == $rto->current_status){ ?>

                                  <form action="/rto/receive/{{ $rto->id }}" method="POST" class="pull-right" onsubmit="return confirm('Are you sure you want to confirm that this RTO has been received at HUB?')" >{{ csrf_field() }} {{ method_field('POST') }}<button type="submit" class="btn btn-danger"><em class="fa fa-check" ></em></button></form>

                                  <?php } ?>

                                

                                </td>

                                <td>{{ $rto->awb }}</td>
                                
                                <td>{{ $rto->branch }}</td>

                                <td>{{ $rto->current_status }}</td>

                                <td>{{ $rto->created_at->format('Y-m-d g:i A') }}</td>                            

                                <td>{{ $rto->updated_at->format('Y-m-d g:i A') }}</td>                            

                              </tr>



                      </tbody>

                @empty

                    No bags.



                @endforelse



                    </table>

				<?php
					//if(isset($founds) && !empty($founds))
					$founds = \Session::get('founds');
					if(isset($founds) && !empty($founds))
					{
					   echo '<div class="alert alert-success">
							<strong>Success!</strong> Rto Sucessfully Verified at Hub for Consignments:'; 
							foreach($founds as $found)
								echo $found.', '; 
						  
						echo '</div>';
					}
				?>
				    
                <?php
				
					$notFounds = \Session::get('notFounds');
					if(isset($notFounds) && !empty($notFounds))
					{
						echo '<div class="alert alert-danger">
							No Consignments Found with AWB:'; 
							foreach($notFounds as $notFound)
								echo $notFound.', ';
						echo '</div>';
					} 
				?>
                
                <?php
					$has_errors = \Session::get('has_errors');
					if(isset($has_errors) && !empty($has_errors))
					{
						echo '<div class="alert alert-warning">
							<strong>Warning!</strong> RTO can\'t be in transit to HUb for Consignment with AWB :'; 
							foreach($has_errors as $has_error)
								echo $has_error.', ';
						echo 'Reason : Steps missing! Please check these consignments History for details.
						</div>';
					}   
                ?>  
              </div>

              <div class="panel-footer">



                <div class="row">

                  

                  <div class="col col-xs-4">Page {{ $rto_consignments->currentPage() }} of {{ $rto_consignments->lastPage()  }}

                  </div>

                  <div class="col col-xs-8 pull-right">

                    <ul class="pagination hidden-xs pull-right">



                       {{ $rto_consignments->appends(request()->input())->links() }} 



                    </ul>



                  </div>

                </div>

              </div>

            </div>



    </div>

</div>





<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>

<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>



  <script>

$.noConflict();  //Not to conflict with other scripts

jQuery(document).ready(function($) {





      var getUrlParameter = function getUrlParameter(sParam) {

      var sPageURL = decodeURIComponent(window.location.search.substring(1)),

          sURLVariables = sPageURL.split('&'),

          sParameterName,

          i;



      for (i = 0; i < sURLVariables.length; i++) {

          sParameterName = sURLVariables[i].split('=');



          if (sParameterName[0] === sParam) {

              return sParameterName[1] === undefined ? true : sParameterName[1];

          }

      }

  };



if(getUrlParameter('date')){

var date = getUrlParameter('date');



}else{

var date = new Date();

}





    $( "#datepicker" ).datepicker({

        changeMonth: true,

        changeYear: true,

        dateFormat : 'yy-mm-dd',

        defaultDate: new Date(),

    }).datepicker("setDate", date);

  } );

  </script>
<script>
    var timer;
    function up() {
        timer = setTimeout(function() { 
            var keywords = $('#search_box').val(); 
            /*var param = $('#search_param').val();*/ 
			//alert(keywords);
            if (keywords.length > 0) { 
              $("#loading").show();
              $.ajax({
                  url: '/ajax-search-rto-consignments',
                  method: 'GET',
                  data:{keywords: keywords}
              }).done(function(response){
                  $("#loading").hide();
                  $('#search-results').html(response);          
              });
            }
        }, 500);
    }

    function down() {
        clearTimeout(timer);
    }
</script>
		

@endsection