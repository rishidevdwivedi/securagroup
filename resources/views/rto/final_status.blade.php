@extends('layouts.app')
@section('content')
<script>
   function getComboA(selectObject) {
       var value = selectObject.value;  
       if(value == 'Return Delivered'){
   
       //Show
             document.getElementById('remarks-delivered').style.display = 'block';
   
       //Hide
             document.getElementById('remarks-others').style.display = 'none';
   
       //Active
             document.getElementById("remarks-delivered").disabled = false;
   
       //Inactive
             document.getElementById("remarks-others").disabled = true;
   
       }else{
   
       //Show
             document.getElementById('remarks-others').style.display = 'block';
   
       //Hide
             document.getElementById('remarks-delivered').style.display = 'none';
   
       //Active
             document.getElementById("remarks-others").disabled = false;          
   
       //Inactive
             document.getElementById("remarks-delivered").disabled = true;
   
       }
   }
</script>
<div class="container-fluid">
   <div id="page-wrapper" class="page-wrapper-cls">
      <div id="page-inner">
         <div class="row">
            <div class="col-md-12">
               <div class="alert alert-warning">
                  <div class="panel panel-default">
                     <div class="panel-heading">
                        HUB : Final RTO Status
                     </div>
                     <div class="panel-body">
                        <form class="form-horizontal" role="form" method="POST" action="/rto/final-status-process">
                           {{ csrf_field() }}
                           <input type="hidden" name="action"     value="show" />
                           <div class="form-group">
                              <label for="exampleInputEmail1" class="col-md-4 control-label">Status</label> 
                              <div class="col-md-6">
                                 <select id="status"  onchange="getComboA(this)"  class="form-control" name="status" value="{{ old('status') }}" required autofocus>
                                    <option value="" >Please Select Consignment Status</option>
                                    <option value="Return Delivered" title="" >Return Delivered</option>
                                    <option value="Returned Undeliverd" title="" >Returned Undeliverd</option>
                                 </select>
                              </div>
                           </div>
                           <div class="form-group{{ $errors->has('remarks') ? ' has-error' : '' }}">
                              <label for="remarks" class="col-md-4 control-label">Remarks</label>
                              <div class="col-md-6">
                                 <textarea id="remarks-delivered" class="form-control" name="remarks" placeholder="optional"  autofocus>{{ old('remarks') }}</textarea>
                                 <select id="remarks-others"   class="form-control" name="remarks" value="{{ old('remarks') }}" style="display:none;" required autofocus>
                                    <option value="" >Please Select Undelivered remarks</option>
                                    <option value="Damaged Shipment" title="" >Damaged Shipment</option>
                                    <option value="RTO Lost In Transit by Branch" title="" >RTO Lost In Transit by Branch</option>
                                    <option value="RTO Lost In Transit by Hub" title="" >RTO Lost In Transit by Hub</option>
                                 </select>
                                 @if ($errors->has('remarks'))
                                 <span class="help-block">
                                 <strong>{{ $errors->first('remarks') }}</strong>
                                 </span>
                                 @endif
                              </div>
                           </div>
                           <div class="form-group">
                              <label for="exampleInputEmail1" class="col-md-4 control-label">Waybill</label> 
                              <div class="col-md-6">
                                 <textarea name="awbs" rows="2" cols="20" id="ctl00_ContentPlaceHolder1_txtwaybill" class="form-control" placeholder="Scan waybill and press submit button." style="height:150px;" required></textarea><br>
                              </div>
                           </div>
                           <div class="form-group">
                              <div class="col-md-6 pull-right">
                                 <input type="submit" name="ctl00$ContentPlaceHolder1$btnsbmit" value="Submit" onclick="javascript:WebForm_DoPostBackWithOptions(new WebForm_PostBackOptions(&quot;ctl00$ContentPlaceHolder1$btnsbmit&quot;, &quot;&quot;, true, &quot;&quot;, &quot;&quot;, false, false))" id="ctl00_ContentPlaceHolder1_btnsbmit" class="btn btn-info">
                              </div>
                           </div>
                        </form>
                     </div>
                  </div>
                  <div class="panel panel-default">
                     <div class="panel-heading">
                        Message Log
                     </div>
                     <div class="panel-body">
                        @if(isset($notFounds) && !empty($notFounds))
                        	<div class="alert alert-danger">Consignment Not Found for Waybill no: 
                            @foreach($notFounds as $notFound)
                            	{{ $notFound }},
                            @endforeach </div>
                         @endif 
                         
                         @if(isset($hasErrors) && !empty($hasErrors))
                        	<div class="alert alert-warning">RTO Final satatus can't be updated for Consignment with AWB : 
                            @foreach($hasErrors as $hasError)
                            	{{ $hasError }},
                            @endforeach Please check these consignments History for details</div>
                         @endif
                         
                         @if(isset($founds) && !empty($founds))
                        	<div class="alert alert-success">RTO Final satatus successfully updated for Consignment with AWB : 
                            @foreach($founds as $found)
                            	{{ $found }},
                            @endforeach </div>
                         @endif  
                        <div class="form-group">
                           <span id="ctl00_ContentPlaceHolder1_lblmsg" style="color:Red;"></span>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
</div>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
@endsection